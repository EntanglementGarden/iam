# Entanglement.Garden IAM
Identity and Access Management. Mostly handled by Ory Kratos and Ory Keto.

Some tests require Keto to be running. This will happen automatically in CI, but to run those tests locally use `integration-tests.sh`.
