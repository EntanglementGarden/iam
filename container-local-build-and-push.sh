#!/bin/bash
set -exuo pipefail
branch=$(git rev-parse --abbrev-ref HEAD)
image="codeberg.org/entanglementgarden/iam:${branch}"
make clean
make entanglement-garden-iam
podman build --build-arg "GOPROXY=${GOPROXY:-direct}" -t "${image}" -f - . <<EOF
FROM debian:stable
RUN apt-get update && apt-get install delve
ADD entanglement-garden-iam /entanglement-garden-iam
CMD ["/entanglement-garden-iam"]
EOF

podman push "${image}"
