# Policies

A policy is a document granting certain permissions on certain objects. It can be assigned to a user or an API key.

Users specify policies in json:

```json
{
    "name": "allow creating and viewing networks and instances",
    "objects": [
        "entanglement.garden:rhyzome:instance:*",
        "entanglement.garden:networking:vpc:*"
    ],
    "relationships": [
        "create",
        "list",
        "view"
    ]
}
```

which is parsed, and some data is saved to the database:


Table: `policies`

| id | name | tenant |
|----|------|--------|
| `policy-zzzzzz` | `allow creating and viewing networks and instances` | `default` |


table: `policy_namespaces`

| policy | namespace |
|--------|--------------|
| `policy-zzzzzz` | `EntanglementGarden_RhyzomeInstance` |
| `policy-zzzzzz` | `EntanglementGarden_NetworkingVpc` |

while the actual relationships and objects are stored in Ory Keto. The namespace list is because Keto doesn't have a mechanism to query for related objects without specifying the type.
