// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	"errors"
	"fmt"
	"strings"
	"unicode"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

const EntanglementObjectWildcard = "*"

var title = cases.Title(language.Und)

// ParseEntanglementObjectID parses an Entanglement Object ID into a keto namespace and object
// Entanglement Object ID format:
// <domain>:<extension>:<object-type>:<object identifier>
// * domain is a DNS name associated with the publisher of the extension, to prevent naming conflicts
// * extension is the name of the extension
// * object-type is the name of the object type provided by the extension
// * object identifier is the unique identifier for the particular object, or * for all objects
//
// entanglement.garden:rhyzome:instance:inst-aaaaaa -> EntanglementGarden_RhyzomeInstance
// example.xyz:coolbeans:bean:* -> ExampleXyz_CoolbeansBean
func ParseEntanglementObjectID(entanglementObject string) (namespace string, object string, err error) {
	entanglementObject = strings.ToLower(entanglementObject) // just in case

	parts := strings.SplitN(entanglementObject, ":", 4)
	if len(parts) != 4 {
		return "", "", errors.New("invalid object: wrong number of parts (must have exactly 4 parts)")
	}

	domain := DomainToCamelCase(parts[0])
	extension := title.String(parts[1])
	objectType := ParseEntanglementIdentifier(title.String(parts[2]))
	object = parts[3]

	if object == EntanglementObjectWildcard {
		object = KetoObjectWildcard
	}

	namespace = fmt.Sprintf("%s_%s%s", domain, extension, objectType)
	return
}

// removes dots and capitalizes the following character
// entanglement.garden -> EntanglementGarden
// example.xyz -> ExampleXyz
func DomainToCamelCase(domain string) string {
	parts := strings.Split(domain, ".")

	for i, p := range parts {
		parts[i] = title.String(p)
	}

	return strings.Join(parts, "")
}

// Creates an EntanglementObjectID from a keto namespace and object name
// EntanglementGarden_RhyzomeInstance inst-aaaaaa -> entanglement.garden:rhyzome:instance:inst-aaaaaa
// ExampleXyz_CoolbeansBean all -> example.xyz:coolbeans:bean:*
func CreateEntanglementObjectID(ketoNamespace string, ketoObject string) string {
	var domain strings.Builder
	var extension strings.Builder
	var objectType strings.Builder

	current := "domain"

	for _, c := range ketoNamespace {
		switch current {
		case "domain":
			if c == '_' {
				current = "extension"
				continue
			}

			if unicode.IsUpper(c) && domain.Len() > 0 {
				domain.WriteRune('.')
			}

			domain.WriteRune(unicode.ToLower(c))

		case "extension":
			if unicode.IsUpper(c) && extension.Len() > 0 {
				current = "objectType"
				objectType.WriteRune(unicode.ToLower(c))
				continue
			}

			extension.WriteRune(unicode.ToLower(c))

		case "objectType":
			objectType.WriteRune(unicode.ToLower(c))
		}
	}

	entanglementObjectType := GetEntanglementIdentifier(objectType.String())

	var entanglementObject string
	if ketoObject == KetoObjectWildcard {
		entanglementObject = EntanglementObjectWildcard
	} else {
		entanglementObject = GetEntanglementIdentifier(ketoObject)
	}

	return fmt.Sprintf("%s:%s:%s:%s", &domain, &extension, entanglementObjectType, entanglementObject)
}

func ParseEntanglementIdentifier(permission string) string {
	return strings.ReplaceAll(permission, "-", "_")
}

func GetEntanglementIdentifier(ketoPermission string) string {
	return strings.ReplaceAll(ketoPermission, "_", "-")
}
