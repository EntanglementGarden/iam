// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox_test

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	client "github.com/ory/keto-client-go"
	"github.com/stretchr/testify/assert"

	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/ketox"
)

func TestSuperUserAssignPolicies(t *testing.T) {
	ctx := context.Background()

	// register an instance
	instance := fmt.Sprintf("entanglement.garden:rhyzome:instance:%s", identifiers.GenerateID(identifiers.PrefixInstance))
	err := ketox.AddDeleteObject(ctx, instance, true)
	assert.NoError(t, err, "add object")

	// create a policy granting access to the instance
	policy := db.UpsertPolicyParams{
		ID:   identifiers.GenerateID(identifiers.PrefixPolicy),
		Name: "test policy",
	}
	policyRelationships := []string{"create", "list", "read"}
	policyObjects := []string{instance}
	err = ketox.UpsertPolicy(ctx, policy, policyRelationships, policyObjects, superuserUserID)
	assert.NoError(t, err, "initial policy insert")

	// some debugging
	expand(instance, policyRelationships[0])
	// expand(instance, "parent")
	expand("entanglement.garden:rhyzome:instance:*", "create")
	// logrus.Debug("superuser ID: ", superuserUserID)

	user := uuid.Must(uuid.NewRandom()).String()
	c := httpx.GetTestContext(&httpx.JWTClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject: superuserUserID,
		},
	})
	err = ketox.ChangeUserPolicies(c, user, []string{policy.ID}, []string{})
	assert.NoError(t, err, "update policy")
}

func expand(objectID string, permission string) {
	namespace, object, _ := ketox.ParseEntanglementObjectID(objectID)
	r := config.C.Keto(false).PermissionApi.ExpandPermissions(context.Background())
	r = r.Namespace(namespace)
	r = r.Object(object)
	r = r.Relation(permission)
	tree, _, _ := r.Execute()
	j, _ := json.MarshalIndent(tree, "", "  ")
	fmt.Println("expanding", objectID, permission)
	fmt.Println(string(j))
}

// verifies that superusers have access to the * of a namespace
func TestSuperUserAccessWildcard(t *testing.T) {
	ctx := context.Background()

	namespace, object, err := ketox.ParseEntanglementObjectID("entanglement.garden:rhyzome:instance:*")
	assert.NoError(t, err)
	relationships := []client.Relationship{
		{
			Namespace: namespace,
			Object:    object,
			Relation:  "create",
		},
	}
	err = ketox.CheckPermission(ctx, relationships, superuserUserID)
	assert.NoError(t, err)
}
