// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	"context"
	"fmt"

	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
)

const (
	KetoNamespacePolicy      = "Policy"
	KetoRelationPolicyMember = "members"

	KetoNamespaceUser  = "User"
	KetoNamespaceGroup = "Group"
)

var (
	patchActionDelete = "delete"
	patchActionInsert = "insert"
)

// Ensures that a policy exists and matches the provided state
// desired ID and metadata are upserted in the database if needed
func UpsertPolicy(ctx context.Context, params db.UpsertPolicyParams, relationshipList []string, objectList []string, requester string) error {
	ctx, log := logging.LogWithField(ctx, "policy", params.ID)

	log.WithField("objects", objectList).WithField("relations", relationshipList).Trace("upserting policy")

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	tx, err := dbc.Begin()
	if err != nil {
		return err
	}
	defer db.RollbackTx(tx)
	qtx := queries.WithTx(tx)

	if err := qtx.UpsertPolicy(ctx, params); err != nil {
		return err
	}

	relationships := map[string]bool{}
	for _, r := range relationshipList {
		relationships[ParseEntanglementIdentifier(r)] = true
	}

	objects := map[string]bool{}
	for _, o := range objectList {
		objects[o] = true
	}

	existing := relationshipMap{}
	patches := []client.RelationshipPatch{}

	log.Trace("querying current namespaces for policy")
	currentNamespaces, err := qtx.GetPolicyNamespaces(ctx, params.ID)
	if err != nil {
		return fmt.Errorf("error getting namespaces for policy %s: %v", params.ID, err)
	}

	log.Debug("policy currently has objects in namespaces: ", currentNamespaces)

	unneededNamespaces := map[string]bool{}

	for _, namespace := range currentNamespaces {
		log.WithField("namespace", namespace).Trace("checking existing relationships")
		// Get all relationships that already exist for this policy
		q := config.C.Keto(false).RelationshipApi.GetRelationships(ctx)
		q = q.Namespace(namespace)
		q = q.SubjectSetNamespace(KetoNamespacePolicy)
		q = q.SubjectSetRelation(KetoRelationPolicyMember)
		q = q.SubjectSetObject(params.ID)
		existingRelationshipsAPIResponse, _, err := q.Execute()
		if err != nil {
			return fmt.Errorf("error querying existing relationships: %v", err)
		}

		for _, tuple := range existingRelationshipsAPIResponse.RelationTuples {
			existing.Add(&tuple)

			log.WithField("tuple", tuple).Trace("existing relationship")

			entanglementObjectID := CreateEntanglementObjectID(namespace, tuple.Object)
			if !relationships[tuple.Relation] || !objects[entanglementObjectID] {
				// this existing relationship was not in the desired state of the policy
				log.Debug("removing ", tuple.Relation, "/", entanglementObjectID)
				t := tuple // copy tuple so we can make a pointer to it that doesn't change value every loop iteration

				patches = append(patches, client.RelationshipPatch{
					Action:        &patchActionDelete,
					RelationTuple: &t,
				})
			}
		}
	}

	checks := []client.Relationship{}
	for _, patch := range patches {
		checks = append(checks, *patch.RelationTuple)
	}

	err = CheckPermission(ctx, checks, requester)
	if err != nil {
		return fmt.Errorf("error verifying requester's access to modified permissions: %v", err)
	}

	createdNamespaces := map[string]bool{}
	for _, entanglementRelationship := range relationshipList {
		relationship := ParseEntanglementIdentifier(entanglementRelationship)
		for _, object := range objectList {
			namespace, ketoObject, err := ParseEntanglementObjectID(object)
			if err != nil {
				return fmt.Errorf("error parsing objectID %s: %v", object, err)
			}

			delete(unneededNamespaces, namespace)

			if !createdNamespaces[namespace] {
				log.Debug("ensuring namespace associated with this policy: ", namespace)
				err = qtx.EnsurePolicyNamespace(ctx, db.EnsurePolicyNamespaceParams{
					Policy:    params.ID,
					Namespace: namespace,
				})
				if err != nil {
					return fmt.Errorf("error ensuring policy namespace: %v", err)
				}

				createdNamespaces[namespace] = true
			}

			if existing.Has(client.NewRelationship(namespace, object, relationship)) {
				log.Debug("policy already has relationship: ", object, "/", relationship)
				continue
			}

			log.Debug("adding to policy: ", object, "/", relationship)
			patches = append(patches, client.RelationshipPatch{
				Action: &patchActionInsert,
				RelationTuple: &client.Relationship{
					Namespace: namespace,
					Relation:  relationship,
					Object:    ketoObject,
					SubjectSet: &client.SubjectSet{
						Namespace: KetoNamespacePolicy,
						Relation:  KetoRelationPolicyMember,
						Object:    params.ID,
					},
				},
			})
		}
	}

	for namespace := range unneededNamespaces {
		log.WithField("namespace", namespace).Trace("removing namespace that is no longer associated with this policy")
		err = qtx.DeletePolicyNamespace(ctx, db.DeletePolicyNamespaceParams{
			Policy:    params.ID,
			Namespace: namespace,
		})
		if err != nil {
			return fmt.Errorf("error deleting namespace from policy: %v", err)
		}
	}

	for _, patch := range patches {
		log.WithFields(logrus.Fields{
			"action":           patch.Action,
			"keto_namespace":   patch.RelationTuple.Namespace,
			"keto_object":      patch.RelationTuple.Object,
			"keto_relation":    patch.RelationTuple.Relation,
			"keto_subject_set": patch.RelationTuple.SubjectSet,
		}).Trace("sending patches to keto")
	}
	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return fmt.Errorf("error applying patch to keto: %v", err)
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func DeletePolicy(ctx context.Context, policyID string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	tx, err := dbc.Begin()
	if err != nil {
		return err
	}
	defer db.RollbackTx(tx)

	qtx := queries.WithTx(tx)

	patches := []client.RelationshipPatch{}

	namespaces, err := qtx.GetPolicyNamespaces(ctx, policyID)
	if err != nil {
		return fmt.Errorf("error getting namespaces for policy %s: %v", policyID, err)
	}

	for _, namespace := range namespaces {
		// Get all relationships that already exist for this policy
		q := config.C.Keto(false).RelationshipApi.GetRelationships(ctx)
		q = q.Namespace(namespace)
		q = q.SubjectSetNamespace(KetoNamespacePolicy)
		q = q.SubjectSetRelation(KetoRelationPolicyMember)
		q = q.SubjectSetObject(policyID)
		existingRelationshipsAPIResponse, _, err := q.Execute()
		if err != nil {
			return fmt.Errorf("error querying existing relationships: %v", err)
		}

		for _, tuple := range existingRelationshipsAPIResponse.RelationTuples {
			// this existing relationship was not in the policy
			patches = append(patches, client.RelationshipPatch{
				Action:        &patchActionDelete,
				RelationTuple: &tuple,
			})
		}

		err = qtx.DeletePolicyNamespace(ctx, db.DeletePolicyNamespaceParams{
			Policy:    policyID,
			Namespace: namespace,
		})
		if err != nil {
			return fmt.Errorf("error deleting policy namespace: %v", err)
		}
	}

	if err := qtx.DeletePolicy(ctx, policyID); err != nil {
		return err
	}

	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return fmt.Errorf("error deleting relationships from keto: %v", err)
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func GetPolicy(ctx context.Context, policyID string) (db.Policy, []string, []string, error) {
	queries, dbc, err := db.Get()
	if err != nil {
		return db.Policy{}, nil, nil, err
	}
	defer dbc.Close()

	policy, err := queries.GetPolicy(ctx, policyID)
	if err != nil {
		return policy, nil, nil, err
	}

	namespaces, err := queries.GetPolicyNamespaces(ctx, policyID)
	if err != nil {
		return policy, nil, nil, fmt.Errorf("error getting namespaces for policy %s: %v", policyID, err)
	}

	objects := map[string]bool{}
	relations := map[string]bool{}
	for _, namespace := range namespaces {
		// Get all relationships that already exist for this policy
		q := config.C.Keto(false).RelationshipApi.GetRelationships(ctx)
		q = q.Namespace(namespace)
		q = q.SubjectSetNamespace(KetoNamespacePolicy)
		q = q.SubjectSetRelation(KetoRelationPolicyMember)
		q = q.SubjectSetObject(policyID)
		existingRelationshipsAPIResponse, _, err := q.Execute()
		if err != nil {
			return policy, nil, nil, fmt.Errorf("error querying existing relationships: %v", err)
		}

		for _, tuple := range existingRelationshipsAPIResponse.RelationTuples {
			object := CreateEntanglementObjectID(tuple.Namespace, tuple.Object)
			objects[object] = true
			relations[tuple.Relation] = true
		}
	}

	relationsList := make([]string, 0, len(relations))
	for r := range relations {
		relationsList = append(relationsList, r)
	}

	objectList := make([]string, 0, len(objects))
	for o := range objects {
		objectList = append(objectList, o)
	}

	return policy, relationsList, objectList, err
}
