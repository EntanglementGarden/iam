// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	echo "github.com/labstack/echo/v4"
	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
)

func GetUserPolicies(ctx context.Context, user string) ([]string, error) {
	q := config.C.Keto(false).RelationshipApi.GetRelationships(ctx)
	q = q.Namespace(KetoNamespacePolicy)
	q = q.Relation(KetoRelationPolicyMember)
	q = q.SubjectId(user)
	existingRelationshipsAPIResponse, _, err := q.Execute()
	if err != nil {
		return nil, fmt.Errorf("error querying relationships from keto: %v", err)
	}

	result := []string{}
	for _, existing := range existingRelationshipsAPIResponse.RelationTuples {
		result = append(result, existing.Object)
	}

	return result, nil
}

func ChangeUserPolicies(c echo.Context, user string, add []string, remove []string) error {
	ctx := c.Request().Context()

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	// iterate over all adds and removes, get a list of all permissions being requested
	patches := []client.RelationshipPatch{}
	permissions := relationshipMap{}
	for _, policy := range add {
		tuples, err := getPolicyTuples(ctx, queries, policy)
		if err != nil {
			return err
		}

		permissions.AddAll(tuples)

		patches = append(patches, client.RelationshipPatch{
			Action: &patchActionInsert,
			RelationTuple: &client.Relationship{
				Namespace: KetoNamespacePolicy,
				Relation:  KetoRelationPolicyMember,
				Object:    policy,
				SubjectId: &user,
			},
		})
	}

	for _, policy := range remove {
		tuples, err := getPolicyTuples(ctx, queries, policy)
		if err != nil {
			return err
		}

		permissions.AddAll(tuples)

		patches = append(patches, client.RelationshipPatch{
			Action: &patchActionDelete,
			RelationTuple: &client.Relationship{
				Namespace: KetoNamespacePolicy,
				Relation:  KetoRelationPolicyMember,
				Object:    policy,
				SubjectId: &user,
			},
		})
	}

	// check if the user or key making the request has all the permissions they are trying to add/remove
	requester, err := httpx.GetJWTClaims(ctx)
	if err != nil {
		return fmt.Errorf("error getting requesting user: %v", err)
	}

	err = CheckPermission(ctx, permissions.GetAllTuples(), requester.Subject)
	if err != nil {
		return err
	}

	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return fmt.Errorf("error applying patch to keto: %v", err)
	}

	return nil
}

func getPolicyTuples(ctx context.Context, queries *db.Queries, policy string) ([]client.Relationship, error) {
	namespaces, err := queries.GetPolicyNamespaces(ctx, policy)
	if err != nil {
		return nil, fmt.Errorf("error getting namespaces for policy %s: %v", policy, err)
	}

	tuples := []client.Relationship{}
	for _, namespace := range namespaces {
		// Get all relationships that already exist for this policy
		q := config.C.Keto(false).RelationshipApi.GetRelationships(ctx)
		q = q.Namespace(namespace)
		q = q.SubjectSetNamespace(KetoNamespacePolicy)
		q = q.SubjectSetRelation(KetoRelationPolicyMember)
		q = q.SubjectSetObject(policy)
		existingRelationshipsAPIResponse, _, err := q.Execute()
		if err != nil {
			return nil, fmt.Errorf("error querying existing relationships: %v", err)
		}

		tuples = append(tuples, existingRelationshipsAPIResponse.RelationTuples...)

	}

	return tuples, nil
}

func CheckPermission(ctx context.Context, tuples []client.Relationship, requester string) error {
	rejects := []string{}
	for _, tuple := range tuples {
		allowed, err := check(ctx, tuple, requester)
		if err != nil {
			return fmt.Errorf("error verifying access to requested permissions (this is different from permission denied) %+v for user %s: %v", tuple, requester, err)
		}

		if !allowed {
			entanglementObjectID := CreateEntanglementObjectID(tuple.Namespace, tuple.Object)
			rejects = append(rejects, fmt.Sprintf("%s on %s", GetEntanglementIdentifier(tuple.Relation), entanglementObjectID))
		}
	}

	if len(rejects) > 0 {
		return httpx.HTTPResponseError{
			Status: http.StatusUnauthorized,
			Err:    fmt.Errorf("cannot modify policy for permissions you don't have:\n\n%s", strings.Join(rejects, "\n")),
		}
	}

	return nil
}

func check(ctx context.Context, tuple client.Relationship, userID string) (bool, error) {
	log := logging.GetLog(ctx).WithFields(logrus.Fields{
		"object":     CreateEntanglementObjectID(tuple.Namespace, tuple.Object),
		"permission": tuple.Relation,
		"requester":  userID,
	})

	q := config.C.Keto(false).PermissionApi.CheckPermission(ctx)
	q = q.Namespace(tuple.Namespace)
	q = q.Object(tuple.Object)
	q = q.Relation(tuple.Relation)
	q = q.SubjectId(userID)
	result, r, err := q.Execute()
	if err != nil {
		if r != nil {
			body, _ := io.ReadAll(r.Body)
			log.Debug("error response body: ", string(body))
		}
		return false, err
	}

	if result.Allowed || tuple.Object == KetoObjectWildcard {
		log.WithField("result", result.Allowed).Trace("permission check result returning without checking wildcard")
		return result.Allowed, nil
	}

	result, _, err = q.Object(KetoObjectWildcard).Execute()
	if err != nil {
		if r != nil {
			body, _ := io.ReadAll(r.Body)
			log.Debug("error response body: ", string(body))
		}
		return false, err
	}

	log.WithField("result", result.Allowed).Trace("permission check returned after wildcard check")

	return result.Allowed, nil
}
