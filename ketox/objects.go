// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	"context"
	"fmt"

	"entanglement.garden/iam/config"
	client "github.com/ory/keto-client-go"
)

const KetoRelationParent = "parent"

var KetoObjectWildcard = "all"

func AddDeleteObject(ctx context.Context, objectID string, add bool) error {
	namespace, object, err := ParseEntanglementObjectID(objectID)
	if err != nil {
		return err
	}

	var action string
	if add {
		action = patchActionInsert
	} else {
		action = patchActionDelete
	}

	patches := []client.RelationshipPatch{}
	patches = append(patches, client.RelationshipPatch{
		Action: &action,
		RelationTuple: &client.Relationship{
			Namespace: namespace,
			Object:    object,
			Relation:  KetoRelationParent,
			SubjectSet: &client.SubjectSet{
				Namespace: namespace,
				Object:    KetoObjectWildcard,
			},
		},
	})

	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return fmt.Errorf("error applying patch to keto: %v", err)
	}

	return nil
}
