// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

type Extension struct {
	Domain  string            `json:"domain"`  // a dns domain that this extension is owned by
	Slug    string            `json:"slug"`    // a lower case string to identify this extension
	Objects []ExtensionObject `json:"objects"` // list of object types this extension has
}

type ExtensionObject struct {
	Name        string   `json:"name"`        // a lowercase identifier for this type of object
	Permissions []string `json:"permissions"` // a list of permissions that objects of this type can have
}

// AllExtensions should be compiled from the extension management extension. Until that exists, it's a static list
var AllExtensions = []Extension{
	{
		Domain: "entanglement.garden",
		Slug:   "rhyzome",
		Objects: []ExtensionObject{
			{
				Name:        "instance",
				Permissions: []string{"create", "list", "read", "delete"},
			},
			{
				Name:        "system",
				Permissions: []string{"bootstrap"},
			},
		},
	},
	{
		Domain: "entanglement.garden",
		Slug:   "networking",
		Objects: []ExtensionObject{
			{
				Name:        "network",
				Permissions: []string{"create", "list", "read", "delete", "attach", "detach"},
			},
			{
				Name:        "host",
				Permissions: []string{"create", "list", "read", "delete"},
			},
			{
				Name:        "publicnetwork",
				Permissions: []string{"create", "list", "read", "delete", "allocate"},
			},
			{
				Name:        "publicip",
				Permissions: []string{"create", "list", "read", "delete", "portforward"},
			},
		},
	},
	{
		Domain: "entanglement.garden",
		Slug:   "iam",
		Objects: []ExtensionObject{
			{
				Name:        "user",
				Permissions: []string{"list", "create", "read", "reset-password", "list-policies", "change-policies"},
			},
			{
				Name:        "api-key",
				Permissions: []string{"list", "create", "read", "list-policies", "change-policies"},
			},
			{
				Name:        "policy",
				Permissions: []string{"list", "create", "read", "update"},
			},
			{
				Name:        "object",
				Permissions: []string{"add", "delete"},
			},
		},
	},
	{
		Domain: "entanglement.garden",
		Slug:   "extensions",
		Objects: []ExtensionObject{
			{
				Name:        "webhook",
				Permissions: []string{"register", "unregister"},
			},
		},
	},
}
