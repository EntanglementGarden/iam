// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox_test

import (
	"strings"
	"testing"

	"entanglement.garden/iam/ketox"
	"github.com/stretchr/testify/assert"
)

var (
	domains = map[string]string{
		"entanglement.garden":             "EntanglementGarden",
		"subdomain.example.seattle.wa.us": "SubdomainExampleSeattleWaUs",
	}

	objects = map[string][]string{
		"entanglement.garden:rhyzome:instance:inst-aaaaaa": {"EntanglementGarden_RhyzomeInstance", "inst-aaaaaa"},
		"Entanglement.garDen:rhyZome:INSTANCE:inst-aaaaaa": {"EntanglementGarden_RhyzomeInstance", "inst-aaaaaa"},
		"entanglement.garden:iam:api-key:*":                {"EntanglementGarden_IamApi_Key", "all"},
		"example.xyz:coolbeans:bean:*":                     {"ExampleXyz_CoolbeansBean", "all"},
	}
)

func TestDomainToCamelCase(t *testing.T) {
	for input, expected := range domains {
		assert.Equal(t, expected, ketox.DomainToCamelCase(input))
	}
}

func TestParseEntanglementObjectID(t *testing.T) {
	for input, expected := range objects {
		namespace, object, err := ketox.ParseEntanglementObjectID(input)
		assert.NoError(t, err)
		assert.Equal(t, expected, []string{namespace, object})
	}

	invalid := []string{
		"entanglement.garden:rhyzome:inst-aaaaaa",
		"",
	}

	for _, input := range invalid {
		namespace, object, err := ketox.ParseEntanglementObjectID(input)
		assert.Error(t, err)
		assert.Equal(t, "", namespace)
		assert.Equal(t, "", object)
	}
}

func TestCreateEntanglementObjectID(t *testing.T) {
	for expected, input := range objects {
		result := ketox.CreateEntanglementObjectID(input[0], input[1])
		assert.Equal(t, strings.ToLower(expected), result)
	}
}
