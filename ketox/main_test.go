// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox_test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/ketox"
)

var superuserUserID = uuid.Must(uuid.NewRandom()).String()

func TestMain(m *testing.M) {
	ketoReadServer := os.Getenv("KETO_PORT_4466_TCP_ADDR")
	ketoWriteServer := os.Getenv("KETO_PORT_4467_TCP_ADDR")
	if ketoReadServer == "" || ketoWriteServer == "" {
		fmt.Println("skipping keto integration tests because keto server IP is not set")
		os.Exit(m.Run())
	}

	config.C.KetoReadServer = client.ServerConfigurations{{URL: fmt.Sprintf("http://%s:4466", ketoReadServer)}}
	config.C.KetoWriteServer = client.ServerConfigurations{{URL: fmt.Sprintf("http://%s:4467", ketoWriteServer)}}
	// config.C.Superusers = []string{superuserUserID}

	ketox.EnsureSuperuser(context.Background())

	tmpdir, err := os.MkdirTemp("", "entanglement-iam-test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tmpdir)

	config.C.SqlitePath = fmt.Sprintf("%s/iam.db", tmpdir)
	logrus.SetLevel(logrus.DebugLevel)

	if err := db.Migrate(); err != nil {
		panic(err)
	}

	code := m.Run()

	os.Exit(code)
}
