// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	client "github.com/ory/keto-client-go"
)

type relationshipMap map[string]map[string]map[string]bool

func (r relationshipMap) Add(tuple *client.Relationship) {
	if r[tuple.Namespace] == nil {
		r[tuple.Namespace] = map[string]map[string]bool{}
	}

	if r[tuple.Namespace][tuple.Relation] == nil {
		r[tuple.Namespace][tuple.Relation] = map[string]bool{}
	}

	r[tuple.Namespace][tuple.Relation][tuple.Object] = true
}

func (r relationshipMap) AddAll(tuples []client.Relationship) {
	for _, t := range tuples {
		r.Add(&t)
	}
}

func (r relationshipMap) Has(tuple *client.Relationship) bool {
	return r[tuple.Namespace][tuple.Relation][tuple.Object]
}

func (r relationshipMap) GetAllTuples() []client.Relationship {
	tuples := []client.Relationship{}

	for namespace := range r {
		for relation := range r[namespace] {
			for object := range r[namespace][relation] {
				tuples = append(tuples, client.Relationship{
					Namespace: namespace,
					Relation:  relation,
					Object:    object,
				})
			}
		}
	}

	return tuples
}
