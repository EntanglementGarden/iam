// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox

import (
	"context"
	"fmt"
	"os"

	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/logging"
	"entanglement.garden/common/retry"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
)

const (
	KetoGroupSuperuser      = "superuser"
	KetoRelationGroupMember = "members"
)

func EnsureSuperuser(ctx context.Context) {
	// Add all configured superusers to the superuser group
	retryer := retry.New()
	for {
		if err := ensuerSuperuserInGroup(ctx); err != nil {
			retryer.Retry(ctx, err)
			continue
		}
		break
	}

	retryer = retry.New()
	for {
		if err := ensureSuperuserPermissions(ctx); err != nil {
			retryer.Retry(ctx, err)
			continue
		}
		break
	}
}

func ensuerSuperuserInGroup(ctx context.Context) error {
	log := logging.GetLog(ctx)

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	superusers, err := queries.SuperuserGetAll(ctx)
	if err != nil {
		return err
	}

	if len(superusers) == 0 {
		log.Fatal("zero superusers in database, refusing to start (this is expected during cluster creation/first boot)")
		os.Exit(1)
	}

	log.WithField("superusers", len(superusers)).Info("ensuring superusers in group")

	patches := []client.RelationshipPatch{}
	for _, user := range superusers {
		logrus.WithField("user", user).Debug("ensuring user has superuser permission")
		u := user
		patches = append(patches, client.RelationshipPatch{
			Action: &patchActionInsert,
			RelationTuple: &client.Relationship{
				Namespace: KetoNamespaceGroup,
				Object:    KetoGroupSuperuser,
				Relation:  KetoRelationGroupMember,
				SubjectId: &u,
			},
		})
	}

	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return err
	}

	logrus.Info("superusers have been successfully added to the supeuser group (or were already there)")

	return nil
}

func ensureSuperuserPermissions(ctx context.Context) error {
	patches := []client.RelationshipPatch{}

	for _, extension := range AllExtensions {
		for _, object := range extension.Objects {
			for _, permission := range object.Permissions {
				objectID := fmt.Sprintf("%s:%s:%s:*", extension.Domain, extension.Slug, object.Name)
				ns, _, err := ParseEntanglementObjectID(objectID)
				if err != nil {
					return fmt.Errorf("error parsing object ID %s: %v", objectID, err)
				}
				permission = ParseEntanglementIdentifier(permission)

				logrus.WithFields(logrus.Fields{
					"entanglement_object": objectID,
					"permission":          permission,
					"keto_namespace":      ns,
				}).Trace("granting permission to superusers")

				patches = append(patches, client.RelationshipPatch{
					Action: &patchActionInsert,
					RelationTuple: &client.Relationship{
						Namespace: ns,
						Object:    KetoObjectWildcard,
						Relation:  permission,
						SubjectSet: &client.SubjectSet{
							Namespace: KetoNamespaceGroup,
							Relation:  KetoRelationGroupMember,
							Object:    KetoGroupSuperuser,
						},
					},
				})
			}
		}
	}

	_, err := config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch(patches).Execute()
	if err != nil {
		return err
	}

	logrus.Info("superuser permissions have been set successfully")

	return nil
}
