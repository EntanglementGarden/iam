// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"entanglement.garden/common/identifiers"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/ketox"
)

var (
	patchActionInsert = "insert"
)

// test creates a new policy, updates it, and verifies that the update worked
func TestPolicyUpsert(t *testing.T) {
	ctx := context.Background()

	initialPolicy := db.UpsertPolicyParams{
		ID:   identifiers.GenerateID(identifiers.PrefixPolicy),
		Name: "test policy",
	}
	initialPolicyRelationships := []string{"create", "list", "read"}
	initialPolicyObjects := []string{"entanglement.garden:rhyzome:instance:*"}
	err := ketox.UpsertPolicy(ctx, initialPolicy, initialPolicyRelationships, initialPolicyObjects, superuserUserID)
	assert.NoError(t, err, "initial policy insert")

	newPolicyRelationships := []string{"create", "delete", "update"}
	newPolicyObjects := []string{"entanglement.garden:rhyzome:instance:" + identifiers.GenerateID(identifiers.PrefixInstance)}
	params := db.UpsertPolicyParams{
		ID:   initialPolicy.ID,
		Name: "testier policier",
	}
	err = ketox.UpsertPolicy(ctx, params, newPolicyRelationships, newPolicyObjects, superuserUserID)
	assert.NoError(t, err, "policy update")

	_, relationsList, objectList, err := ketox.GetPolicy(ctx, initialPolicy.ID)
	assert.NoError(t, err, "get policy")
	for _, r := range newPolicyRelationships {
		assert.Contains(t, relationsList, r)
	}
	for _, o := range newPolicyObjects {
		assert.Contains(t, objectList, o)
	}
	assert.Equal(t, newPolicyObjects, objectList)
}

func TestPolicyAssignments(t *testing.T) {
	ctx := context.Background()

	// register an instance
	instance := fmt.Sprintf("entanglement.garden:rhyzome:instance:%s", identifiers.GenerateID(identifiers.PrefixInstance))
	err := ketox.AddDeleteObject(ctx, instance, true)
	assert.NoError(t, err, "add object")

	// create a policy granting user access to all instances
	initialPolicy := db.UpsertPolicyParams{
		ID:   identifiers.GenerateID(identifiers.PrefixPolicy),
		Name: "test policy",
	}
	initialPolicyRelationships := []string{"create", "list", "read"}
	initialPolicyObjects := []string{instance}
	err = ketox.UpsertPolicy(ctx, initialPolicy, initialPolicyRelationships, initialPolicyObjects, superuserUserID)
	assert.NoError(t, err, "initial policy insert")

	// assign the policy to a user
	userID := uuid.Must(uuid.NewRandom()).String()
	patch := client.RelationshipPatch{
		Action: &patchActionInsert,
		RelationTuple: &client.Relationship{
			Namespace: ketox.KetoNamespacePolicy,
			Relation:  ketox.KetoRelationPolicyMember,
			Object:    initialPolicy.ID,
			SubjectId: &userID,
		},
	}
	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch([]client.RelationshipPatch{patch}).Execute()
	assert.NoError(t, err)

	namespace, object, err := ketox.ParseEntanglementObjectID(instance)
	assert.NoError(t, err)
	relationships := []client.Relationship{
		{
			Namespace: namespace,
			Object:    object,
			Relation:  "create",
		},
	}
	err = ketox.CheckPermission(ctx, relationships, userID)
	assert.NoError(t, err)
}

func TestPolicyWildcardAssignments(t *testing.T) {
	ctx := context.Background()

	// register an instance
	instance := fmt.Sprintf("entanglement.garden:rhyzome:instance:%s", identifiers.GenerateID(identifiers.PrefixInstance))
	err := ketox.AddDeleteObject(ctx, instance, true)
	assert.NoError(t, err, "add object")

	// create a policy granting user access to all instances
	initialPolicy := db.UpsertPolicyParams{
		ID:   identifiers.GenerateID(identifiers.PrefixPolicy),
		Name: "test policy",
	}
	initialPolicyRelationships := []string{"create", "list", "read"}
	initialPolicyObjects := []string{"entanglement.garden:rhyzome:instance:*"}
	err = ketox.UpsertPolicy(ctx, initialPolicy, initialPolicyRelationships, initialPolicyObjects, superuserUserID)
	assert.NoError(t, err, "initial policy insert")

	// assign the policy to a user
	userID := uuid.Must(uuid.NewRandom()).String()
	patch := client.RelationshipPatch{
		Action: &patchActionInsert,
		RelationTuple: &client.Relationship{
			Namespace: ketox.KetoNamespacePolicy,
			Relation:  ketox.KetoRelationPolicyMember,
			Object:    initialPolicy.ID,
			SubjectId: &userID,
		},
	}
	_, err = config.C.Keto(true).RelationshipApi.PatchRelationships(ctx).RelationshipPatch([]client.RelationshipPatch{patch}).Execute()
	assert.NoError(t, err)

	logrus.Debug("user ID: ", userID)

	// expand("entanglement.garden:rhyzome:instance:*", "create")

	namespace, object, err := ketox.ParseEntanglementObjectID(instance)
	assert.NoError(t, err)
	relationships := []client.Relationship{
		{
			Namespace: namespace,
			Object:    object,
			Relation:  "create",
		},
	}
	err = ketox.CheckPermission(ctx, relationships, userID)
	assert.NoError(t, err)
}

func TestPolicyDelete(t *testing.T) {
	ctx := context.Background()

	policy := db.UpsertPolicyParams{
		ID:   identifiers.GenerateID(identifiers.PrefixPolicy),
		Name: "test policy",
	}
	policyRelationships := []string{"create", "list", "read"}
	policyObjects := []string{"entanglement.garden:rhyzome:instance:*"}
	err := ketox.UpsertPolicy(ctx, policy, policyRelationships, policyObjects, superuserUserID)
	assert.NoError(t, err, "create policy")

	err = ketox.DeletePolicy(ctx, policy.ID)
	assert.NoError(t, err, "delete policy")
}
