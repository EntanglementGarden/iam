// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package ketox_test

import (
	"context"
	"testing"

	"entanglement.garden/iam/ketox"
	"github.com/stretchr/testify/assert"
)

func TestAddDeleteObjec(t *testing.T) {
	ctx := context.Background()

	objectID := "entanglement.garden:rhyzome:instance:inst-aaaaaa"
	err := ketox.AddDeleteObject(ctx, objectID, true)
	assert.NoError(t, err, "add object")

	err = ketox.AddDeleteObject(ctx, objectID, false)
	assert.NoError(t, err, "delete object")
}

func TestAddInvalidObject(t *testing.T) {
	ctx := context.Background()

	objectID := "INVALID OBJECT ID"
	err := ketox.AddDeleteObject(ctx, objectID, true)
	assert.Error(t, err, "add object")
}
