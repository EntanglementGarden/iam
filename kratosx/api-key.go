// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package kratosx

import (
	"context"
	"encoding/json"

	kratos "github.com/ory/kratos-client-go"

	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
)

const SchemaIDRobot = "robot"

func AddApiKey(ctx context.Context, key string, secret string) (string, error) {
	log := logging.GetLog(ctx)

	log.WithField("key", key).WithField("secret", secret).Debug("creating api key")

	k := config.C.Kratos(true)
	body := kratos.CreateIdentityBody{
		Credentials: &kratos.IdentityWithCredentials{
			Password: &kratos.IdentityWithCredentialsPassword{
				Config: &kratos.IdentityWithCredentialsPasswordConfig{
					Password: &secret,
				},
			},
		},
		SchemaId: SchemaIDRobot,
		Traits:   map[string]interface{}{"apikey": key},
	}
	identity, _, err := k.IdentityApi.CreateIdentity(ctx).CreateIdentityBody(body).Execute()
	if err != nil {
		log.Warn("error from Kratos while creating identity")
		return "", err
	}

	if identityJson, err := json.Marshal(identity); err != nil {
		log.WithField("error", err).Warn("error marshaling identity into json")
	} else {
		log.WithField("new_id_json", string(identityJson)).Debug("identity created")
	}

	return identity.Id, nil
}
