// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.20.0

package db

import (
	"database/sql"
)

type Policy struct {
	ID     string
	Name   string
	Tenant sql.NullString
}

type PolicyNamespace struct {
	Policy    string
	Namespace string
}

type Superuser struct {
	ID string
}
