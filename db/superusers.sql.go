// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.20.0
// source: superusers.sql

package db

import (
	"context"
)

const superuserAdd = `-- name: SuperuserAdd :exec
INSERT INTO superusers(id) VALUES (?)
`

func (q *Queries) SuperuserAdd(ctx context.Context, id string) error {
	_, err := q.db.ExecContext(ctx, superuserAdd, id)
	return err
}

const superuserDelete = `-- name: SuperuserDelete :exec
DELETE FROM superusers WHERE id = ?
`

func (q *Queries) SuperuserDelete(ctx context.Context, id string) error {
	_, err := q.db.ExecContext(ctx, superuserDelete, id)
	return err
}

const superuserGetAll = `-- name: SuperuserGetAll :many

SELECT id FROM superusers
`

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
func (q *Queries) SuperuserGetAll(ctx context.Context) ([]string, error) {
	rows, err := q.db.QueryContext(ctx, superuserGetAll)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []string
	for rows.Next() {
		var id string
		if err := rows.Scan(&id); err != nil {
			return nil, err
		}
		items = append(items, id)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
