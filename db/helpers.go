// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"database/sql"
	"embed"

	_ "github.com/mattn/go-sqlite3"
	goose "github.com/pressly/goose/v3"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/iam/config"
)

//go:embed sqlite/migrations
var migrations embed.FS

func Migrate() error {
	log.WithField("database", config.C.SqlitePath).Info("running database migrations")

	_, dbConn, err := Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	goose.SetBaseFS(migrations)

	if err := goose.SetDialect("sqlite3"); err != nil {
		return err
	}

	if err := goose.Up(dbConn, "sqlite/migrations"); err != nil {
		return err
	}

	return nil
}

// Get the database and closable DB object
// example usage:
//
//	  queries, dbConn, err := db.Get()
//	  if err != nil {
//		   return err
//	  }
//	  defer dbConn.Close()
func Get() (*Queries, *sql.DB, error) {
	db, err := sql.Open("sqlite3", config.C.SqlitePath)
	if err != nil {
		return nil, nil, err
	}

	_, err = db.Exec("PRAGMA foreign_keys = ON")
	if err != nil {
		return nil, nil, err
	}

	return New(db), db, nil
}

// NullableString accepts a string and returns an sql.NullString
// if the input string is zero-length, the output will be marked
// null
func NullableString(s string) sql.NullString {
	return sql.NullString{
		Valid:  s != "",
		String: s,
	}
}

func RollbackTx(tx *sql.Tx) {
	if err := tx.Rollback(); err != nil && err != sql.ErrTxDone {
		log.Error("error rolling back transaction: ", err)
	}
}
