-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: UpsertPolicy :exec
INSERT INTO policies (id, name, tenant) VALUES (?, ?, ?) ON CONFLICT DO UPDATE SET name = excluded.name WHERE id = excluded.id AND tenant = excluded.tenant;

-- name: DeletePolicy :exec
DELETE FROM policies WHERE id = ?;

-- name: GetPoliciesByTenant :many
SELECT id, name FROM policies WHERE tenant = ?;

-- name: GetPolicy :one
SELECT * FROM policies WHERE id = ?;

-- name: GetPolicyNamespaces :many
SELECT namespace FROM policy_namespaces WHERE policy = ?;

-- name: DeletePolicyNamespace :exec
DELETE FROM policy_namespaces WHERE policy = ? AND namespace = ?;

-- name: EnsurePolicyNamespace :exec
INSERT INTO policy_namespaces (policy, namespace) VALUES (?, ?);
