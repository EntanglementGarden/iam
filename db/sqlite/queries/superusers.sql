-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: SuperuserGetAll :many
SELECT * FROM superusers;

-- name: SuperuserAdd :exec
INSERT INTO superusers(id) VALUES (?);

-- name: SuperuserDelete :exec
DELETE FROM superusers WHERE id = ?;
