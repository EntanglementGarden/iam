-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- +goose Up
-- +goose StatementBegin
PRAGMA foreign_keys = ON;

CREATE TABLE superusers (
    id TEXT UNIQUE NOT NULL
);

CREATE TABLE policies (
    id TEXT UNIQUE NOT NULL,
    name TEXT NOT NULL,
    tenant TEXT
);

CREATE TABLE policy_namespaces (
    policy TEXT NOT NULL REFERENCES policies(id),
    namespace TEXT NOT NULL,

    UNIQUE(policy, namespace) ON CONFLICT IGNORE
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE superusers;
DROP TABLE policies;
DROP TABLE policy_namespaces;
-- +goose StatementEnd
