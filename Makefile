entanglement-garden-iam: httpserver/web.ewp openapi/openapi.go $(shell find . -name '*.go' -print)
	go build -ldflags "-X entanglement.garden/common/config.Version=$(shell git describe --always --tags)" ./cmd/entanglement-garden-iam

httpserver/web.ewp: web/main.go web/api-keys.go web/users.go web/templates/api-keys.qtpl.go web/templates/policy-list.qtpl.go web/templates/user-create.qtpl.go web/templates/user-details.qtpl.go web/templates/user-list.qtpl.go web/templates/user-recovery.qtpl.go
	go build -trimpath -o httpserver/web.ewp ./web

openapi/openapi.go: openapi/spec.yaml
	eg dev codegen openapi-server openapi/spec.yaml openapi/openapi.go

web/templates/*.qtpl.go: web/templates/*.qtpl
	qtc -dir web/templates

clean:
	-rm httpserver/web.ewp entanglement-garden-iam
