import { Namespace, SubjectSet, Context } from "@ory/keto-namespace-types"

class User implements Namespace {}

class Group implements Namespace {
  related: {
    members: (User | Group)[]
  }
}

class Policy implements Namespace {
  related: {
    members: (User | Group)[]
  }
}

// entanglement.garden:rhyzome:instance:
class EntanglementGarden_RhyzomeInstance implements Namespace {
  related: {
    create: SubjectSet<Policy, "members">[],
    list: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_RhyzomeInstance[],
  }

  permits = {
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
  }
}
// entanglement.garden:rhyzome:system:
class EntanglementGarden_RhyzomeSystem implements Namespace {
  related: {
    bootstrap: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_RhyzomeSystem[],
  }

  permits = {
    bootstrap: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.bootstrap(ctx)) || this.related.bootstrap.includes(ctx.subject),
  }
}
// entanglement.garden:networking:network:
class EntanglementGarden_NetworkingNetwork implements Namespace {
  related: {
    create: SubjectSet<Policy, "members">[],
    list: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    attach: SubjectSet<Policy, "members">[],
    detach: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_NetworkingNetwork[],
  }

  permits = {
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
    attach: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.attach(ctx)) || this.related.attach.includes(ctx.subject),
    detach: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.detach(ctx)) || this.related.detach.includes(ctx.subject),
  }
}
// entanglement.garden:networking:host:
class EntanglementGarden_NetworkingHost implements Namespace {
  related: {
    create: SubjectSet<Policy, "members">[],
    list: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_NetworkingHost[],
  }

  permits = {
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
  }
}
// entanglement.garden:networking:publicnetwork:
class EntanglementGarden_NetworkingPublicnetwork implements Namespace {
  related: {
    create: SubjectSet<Policy, "members">[],
    list: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    allocate: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_NetworkingPublicnetwork[],
  }

  permits = {
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
    allocate: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.allocate(ctx)) || this.related.allocate.includes(ctx.subject),
  }
}
// entanglement.garden:networking:publicip:
class EntanglementGarden_NetworkingPublicip implements Namespace {
  related: {
    create: SubjectSet<Policy, "members">[],
    list: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    portforward: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_NetworkingPublicip[],
  }

  permits = {
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
    portforward: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.portforward(ctx)) || this.related.portforward.includes(ctx.subject),
  }
}
// entanglement.garden:iam:user:
class EntanglementGarden_IamUser implements Namespace {
  related: {
    list: SubjectSet<Policy, "members">[],
    create: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    reset_password: SubjectSet<Policy, "members">[],
    list_policies: SubjectSet<Policy, "members">[],
    change_policies: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_IamUser[],
  }

  permits = {
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    reset_password: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.reset_password(ctx)) || this.related.reset_password.includes(ctx.subject),
    list_policies: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list_policies(ctx)) || this.related.list_policies.includes(ctx.subject),
    change_policies: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.change_policies(ctx)) || this.related.change_policies.includes(ctx.subject),
  }
}
// entanglement.garden:iam:api-key:
class EntanglementGarden_IamApi_Key implements Namespace {
  related: {
    list: SubjectSet<Policy, "members">[],
    create: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    list_policies: SubjectSet<Policy, "members">[],
    change_policies: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_IamApi_Key[],
  }

  permits = {
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    list_policies: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list_policies(ctx)) || this.related.list_policies.includes(ctx.subject),
    change_policies: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.change_policies(ctx)) || this.related.change_policies.includes(ctx.subject),
  }
}
// entanglement.garden:iam:policy:
class EntanglementGarden_IamPolicy implements Namespace {
  related: {
    list: SubjectSet<Policy, "members">[],
    create: SubjectSet<Policy, "members">[],
    read: SubjectSet<Policy, "members">[],
    update: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_IamPolicy[],
  }

  permits = {
    list: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.list(ctx)) || this.related.list.includes(ctx.subject),
    create: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.create(ctx)) || this.related.create.includes(ctx.subject),
    read: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.read(ctx)) || this.related.read.includes(ctx.subject),
    update: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.update(ctx)) || this.related.update.includes(ctx.subject),
  }
}
// entanglement.garden:iam:object:
class EntanglementGarden_IamObject implements Namespace {
  related: {
    add: SubjectSet<Policy, "members">[],
    delete: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_IamObject[],
  }

  permits = {
    add: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.add(ctx)) || this.related.add.includes(ctx.subject),
    delete: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.delete(ctx)) || this.related.delete.includes(ctx.subject),
  }
}
// entanglement.garden:extensions:webhook:
class EntanglementGarden_ExtensionsWebhook implements Namespace {
  related: {
    register: SubjectSet<Policy, "members">[],
    unregister: SubjectSet<Policy, "members">[],
    parent: EntanglementGarden_ExtensionsWebhook[],
  }

  permits = {
    register: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.register(ctx)) || this.related.register.includes(ctx.subject),
    unregister: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.unregister(ctx)) || this.related.unregister.includes(ctx.subject),
  }
}