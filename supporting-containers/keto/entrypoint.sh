#!/bin/sh
set -ex
keto migrate -c /etc/ory/keto/keto.yaml up --yes
keto serve -c /etc/ory/keto/keto.yaml --sqa-opt-out
