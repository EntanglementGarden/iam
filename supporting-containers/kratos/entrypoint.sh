#!/bin/sh
set -ex
sed "s#EG_DOMAIN#${EG_DOMAIN}#g" /etc/ory/kratos/kratos.template.yaml | \
sed "s#RANDOM_64#${RANDOM_64}#g" | \
sed "s#RANDOM_32#${RANDOM_32}#g" > /tmp/kratos.yaml
kratos migrate -c /tmp/kratos.yaml sql -e --yes
kratos serve -c /tmp/kratos.yaml --sqa-opt-out
