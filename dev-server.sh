#!/bin/bash
set -exuo pipefail
DLV_PORT=2345

make
podman build -t iam -f - . <<EOF
FROM debian:stable
RUN apt-get update && apt-get install delve
ADD entanglement-garden-iam /entanglement-garden-iam
CMD ["/entanglement-garden-iam"]
EOF

if [[ "${1:-}" == "-d" ]]; then
    podman run --rm --network entanglement-devtools --name iam --env-file "$(pwd)/../devtools/entanglement-common.env" -v $(pwd)/../devtools/state/iam:/var/entanglement.garden/iam -v $(pwd)/../devtools/entanglement.garden:/etc/entanglement.garden -p "127.0.0.1:${DLV_PORT}:${DLV_PORT}" iam dlv --listen=":${DLV_PORT}" --headless=true --api-version=2 exec /entanglement-garden-iam
else
    podman run --rm --network entanglement-devtools --name iam --env-file "$(pwd)/../devtools/entanglement-common.env" -v $(pwd)/../devtools/state/iam:/var/entanglement.garden/iam -v $(pwd)/../devtools/entanglement.garden:/etc/entanglement.garden iam
fi
