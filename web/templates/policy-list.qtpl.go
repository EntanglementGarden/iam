// Code generated by qtc from "policy-list.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
//

//line web/templates/policy-list.qtpl:4
package templates

//line web/templates/policy-list.qtpl:4
import "entanglement.garden/api-client/egiam"

//line web/templates/policy-list.qtpl:6
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line web/templates/policy-list.qtpl:6
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line web/templates/policy-list.qtpl:7
type Policies struct {
	Policies []egiam.Policy
}

//line web/templates/policy-list.qtpl:12
func (p *Policies) StreamTitle(qw422016 *qt422016.Writer) {
//line web/templates/policy-list.qtpl:12
	qw422016.N().S(`Policies`)
//line web/templates/policy-list.qtpl:12
}

//line web/templates/policy-list.qtpl:12
func (p *Policies) WriteTitle(qq422016 qtio422016.Writer) {
//line web/templates/policy-list.qtpl:12
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/policy-list.qtpl:12
	p.StreamTitle(qw422016)
//line web/templates/policy-list.qtpl:12
	qt422016.ReleaseWriter(qw422016)
//line web/templates/policy-list.qtpl:12
}

//line web/templates/policy-list.qtpl:12
func (p *Policies) Title() string {
//line web/templates/policy-list.qtpl:12
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/policy-list.qtpl:12
	p.WriteTitle(qb422016)
//line web/templates/policy-list.qtpl:12
	qs422016 := string(qb422016.B)
//line web/templates/policy-list.qtpl:12
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/policy-list.qtpl:12
	return qs422016
//line web/templates/policy-list.qtpl:12
}

//line web/templates/policy-list.qtpl:14
func (p *Policies) StreamBody(qw422016 *qt422016.Writer) {
//line web/templates/policy-list.qtpl:14
	qw422016.N().S(`
<h1>Policies</h1>
<ul class="monospace">
`)
//line web/templates/policy-list.qtpl:17
	for _, policy := range p.Policies {
//line web/templates/policy-list.qtpl:17
		qw422016.N().S(`
<li><a href="/entanglement.garden/iam/policies/`)
//line web/templates/policy-list.qtpl:18
		qw422016.E().S(*policy.Id)
//line web/templates/policy-list.qtpl:18
		qw422016.N().S(`">`)
//line web/templates/policy-list.qtpl:18
		qw422016.E().S(policy.Name)
//line web/templates/policy-list.qtpl:18
		qw422016.N().S(`</a></li>
`)
//line web/templates/policy-list.qtpl:19
	}
//line web/templates/policy-list.qtpl:19
	qw422016.N().S(`
</ul>
`)
//line web/templates/policy-list.qtpl:21
}

//line web/templates/policy-list.qtpl:21
func (p *Policies) WriteBody(qq422016 qtio422016.Writer) {
//line web/templates/policy-list.qtpl:21
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/policy-list.qtpl:21
	p.StreamBody(qw422016)
//line web/templates/policy-list.qtpl:21
	qt422016.ReleaseWriter(qw422016)
//line web/templates/policy-list.qtpl:21
}

//line web/templates/policy-list.qtpl:21
func (p *Policies) Body() string {
//line web/templates/policy-list.qtpl:21
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/policy-list.qtpl:21
	p.WriteBody(qb422016)
//line web/templates/policy-list.qtpl:21
	qs422016 := string(qb422016.B)
//line web/templates/policy-list.qtpl:21
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/policy-list.qtpl:21
	return qs422016
//line web/templates/policy-list.qtpl:21
}
