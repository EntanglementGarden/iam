// Code generated by qtc from "user-create.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
//

//line web/templates/user-create.qtpl:4
package templates

//line web/templates/user-create.qtpl:4
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line web/templates/user-create.qtpl:4
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line web/templates/user-create.qtpl:5
type UserCreatePage struct {
}

//line web/templates/user-create.qtpl:9
func (p *UserCreatePage) StreamTitle(qw422016 *qt422016.Writer) {
//line web/templates/user-create.qtpl:9
	qw422016.N().S(`Create user`)
//line web/templates/user-create.qtpl:9
}

//line web/templates/user-create.qtpl:9
func (p *UserCreatePage) WriteTitle(qq422016 qtio422016.Writer) {
//line web/templates/user-create.qtpl:9
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/user-create.qtpl:9
	p.StreamTitle(qw422016)
//line web/templates/user-create.qtpl:9
	qt422016.ReleaseWriter(qw422016)
//line web/templates/user-create.qtpl:9
}

//line web/templates/user-create.qtpl:9
func (p *UserCreatePage) Title() string {
//line web/templates/user-create.qtpl:9
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/user-create.qtpl:9
	p.WriteTitle(qb422016)
//line web/templates/user-create.qtpl:9
	qs422016 := string(qb422016.B)
//line web/templates/user-create.qtpl:9
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/user-create.qtpl:9
	return qs422016
//line web/templates/user-create.qtpl:9
}

//line web/templates/user-create.qtpl:11
func (p *UserCreatePage) StreamBody(qw422016 *qt422016.Writer) {
//line web/templates/user-create.qtpl:11
	qw422016.N().S(`
<h1>create user</h1>
<form method="post">
<table>
    <tr><td>username</td><td><input type="text" name="username" /></td></tr>
    <tr><td>email</td><td><input type="text" name="email" /></td></tr>
    <tr><td colspan="2"><button type="submit">create</button></tr>
</table>
</form>
`)
//line web/templates/user-create.qtpl:20
}

//line web/templates/user-create.qtpl:20
func (p *UserCreatePage) WriteBody(qq422016 qtio422016.Writer) {
//line web/templates/user-create.qtpl:20
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/user-create.qtpl:20
	p.StreamBody(qw422016)
//line web/templates/user-create.qtpl:20
	qt422016.ReleaseWriter(qw422016)
//line web/templates/user-create.qtpl:20
}

//line web/templates/user-create.qtpl:20
func (p *UserCreatePage) Body() string {
//line web/templates/user-create.qtpl:20
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/user-create.qtpl:20
	p.WriteBody(qb422016)
//line web/templates/user-create.qtpl:20
	qs422016 := string(qb422016.B)
//line web/templates/user-create.qtpl:20
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/user-create.qtpl:20
	return qs422016
//line web/templates/user-create.qtpl:20
}
