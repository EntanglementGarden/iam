// Code generated by qtc from "user-details.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
//

//line web/templates/user-details.qtpl:4
package templates

//line web/templates/user-details.qtpl:4
import "entanglement.garden/api-client/egiam"

//line web/templates/user-details.qtpl:6
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line web/templates/user-details.qtpl:6
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line web/templates/user-details.qtpl:7
type UserDetailPage struct {
	User egiam.User
}

//line web/templates/user-details.qtpl:12
func (p *UserDetailPage) StreamTitle(qw422016 *qt422016.Writer) {
//line web/templates/user-details.qtpl:12
	qw422016.N().S(`User `)
//line web/templates/user-details.qtpl:12
	qw422016.E().S(p.User.Username)
//line web/templates/user-details.qtpl:12
}

//line web/templates/user-details.qtpl:12
func (p *UserDetailPage) WriteTitle(qq422016 qtio422016.Writer) {
//line web/templates/user-details.qtpl:12
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/user-details.qtpl:12
	p.StreamTitle(qw422016)
//line web/templates/user-details.qtpl:12
	qt422016.ReleaseWriter(qw422016)
//line web/templates/user-details.qtpl:12
}

//line web/templates/user-details.qtpl:12
func (p *UserDetailPage) Title() string {
//line web/templates/user-details.qtpl:12
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/user-details.qtpl:12
	p.WriteTitle(qb422016)
//line web/templates/user-details.qtpl:12
	qs422016 := string(qb422016.B)
//line web/templates/user-details.qtpl:12
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/user-details.qtpl:12
	return qs422016
//line web/templates/user-details.qtpl:12
}

//line web/templates/user-details.qtpl:14
func (p *UserDetailPage) StreamBody(qw422016 *qt422016.Writer) {
//line web/templates/user-details.qtpl:14
	qw422016.N().S(`
<h1>User `)
//line web/templates/user-details.qtpl:15
	qw422016.E().S(p.User.Username)
//line web/templates/user-details.qtpl:15
	qw422016.N().S(`</h1>
<table>
    <tr><td>username</td><td>`)
//line web/templates/user-details.qtpl:17
	qw422016.E().S(p.User.Username)
//line web/templates/user-details.qtpl:17
	qw422016.N().S(`</td></tr>
    <tr><td>email</td><td>`)
//line web/templates/user-details.qtpl:18
	qw422016.E().S(p.User.Email)
//line web/templates/user-details.qtpl:18
	qw422016.N().S(`</td></tr>
    <tr>
        <td>actions</td>
        <td>
            <ul>
                <li><a href="/entanglement.garden/iam/users/`)
//line web/templates/user-details.qtpl:23
	qw422016.E().S(p.User.Id)
//line web/templates/user-details.qtpl:23
	qw422016.N().S(`/recovery">generate reset link</a></li>
                <li><a href="/entanglement.garden/iam/users/`)
//line web/templates/user-details.qtpl:24
	qw422016.E().S(p.User.Id)
//line web/templates/user-details.qtpl:24
	qw422016.N().S(`/policies">policies</a></li>
            </ul>
        </td>
    </tr>
</table>
`)
//line web/templates/user-details.qtpl:29
}

//line web/templates/user-details.qtpl:29
func (p *UserDetailPage) WriteBody(qq422016 qtio422016.Writer) {
//line web/templates/user-details.qtpl:29
	qw422016 := qt422016.AcquireWriter(qq422016)
//line web/templates/user-details.qtpl:29
	p.StreamBody(qw422016)
//line web/templates/user-details.qtpl:29
	qt422016.ReleaseWriter(qw422016)
//line web/templates/user-details.qtpl:29
}

//line web/templates/user-details.qtpl:29
func (p *UserDetailPage) Body() string {
//line web/templates/user-details.qtpl:29
	qb422016 := qt422016.AcquireByteBuffer()
//line web/templates/user-details.qtpl:29
	p.WriteBody(qb422016)
//line web/templates/user-details.qtpl:29
	qs422016 := string(qb422016.B)
//line web/templates/user-details.qtpl:29
	qt422016.ReleaseByteBuffer(qb422016)
//line web/templates/user-details.qtpl:29
	return qs422016
//line web/templates/user-details.qtpl:29
}
