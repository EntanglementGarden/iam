// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egiam"
	"entanglement.garden/iam/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func UserListPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	allUsers, err := iam.ListAllUsersWithResponse(ctx)
	if err != nil {
		log.Warn("error listing all users from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user list"}
	}

	if allUsers.JSON200 == nil {
		log.Warn("non-successful response getting user list: ", string(allUsers.Body))
		return nil, plugin.Error{Description: "unexpected error getting user list"}
	}

	return plugin.WritePage(ctx, request, &templates.UserListPage{Users: *allUsers.JSON200})
}

func UserCreatePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	if request.Method == http.MethodGet {
		return plugin.WritePage(ctx, request, &templates.UserCreatePage{})
	}

	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, plugin.Error{Err: err}
	}

	var email *string
	if form.Get("email") != "" {
		e := form.Get("email")
		email = &e
	}

	createUser, err := iam.CreateUserWithResponse(ctx, egiam.CreateUserJSONRequestBody{
		Username: form.Get("username"),
		Email:    email,
	})
	if err != nil {
		log.Warn("error creating user: ", err)
		return nil, plugin.Error{Description: "unexpected error creating user"}
	}

	if createUser.JSON201 == nil {
		log.Warn("non-successful response creating user: ", string(createUser.Body))
		return nil, plugin.Error{Description: "unexpected error creating user"}
	}

	return plugin.Redirect(fmt.Sprintf("/iam/users/%s", createUser.JSON201.Id))
}

func UserDetailPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	user, err := iam.GetUserWithResponse(ctx, request.PathParams["id"])
	if err != nil {
		log.Warn("error listing all users from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user list"}
	}

	if user.JSON200 == nil {
		log.Warn("non-successful response getting user: ", string(user.Body))
		return nil, plugin.Error{Description: "unexpected error creating user"}
	}

	return plugin.WritePage(ctx, request, &templates.UserDetailPage{User: *user.JSON200})
}

func UserRecoveryLinkPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	user, err := iam.GetUserWithResponse(ctx, request.PathParams["id"])
	if err != nil {
		log.Warn("error listing all users from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user list"}
	}

	if user.JSON200 == nil {
		log.Warn("non-successful response getting user: ", string(user.Body))
		return nil, plugin.Error{Description: "unexpected error getting user"}
	}

	link, err := iam.GenerateRecoveryLinkWithResponse(ctx, request.PathParams["id"])
	if err != nil {
		log.Warn("error listing all users from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user list"}
	}

	if link.JSON201 == nil {
		log.Warn("non-successful response generating recovery link: ", string(link.Body))
		return nil, plugin.Error{Description: "unexpected error generating recovery link"}
	}

	return plugin.WritePage(ctx, request, &templates.UserRecoveryPage{
		User: *user.JSON200,
		Link: *link.JSON201,
	})
}
