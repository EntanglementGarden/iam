// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"net/http"

	"entanglement.garden/api-client/egiam"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	"github.com/sirupsen/logrus"
)

var apiConnectError = plugin.Error{Description: "unexpected error preparing connection to IAM service"}

var p = plugin.Plugin{
	Name:      "Identity & Access Management",
	Extension: "entanglement.garden/iam",
	Endpoints: map[string]map[string]plugin.Endpoint{
		"/": {
			http.MethodGet: plugin.AlwaysRedirect("/users"),
		},
		"/users": {
			http.MethodGet: UserListPage,
		},
		"/users/create": {
			http.MethodGet:  UserCreatePage,
			http.MethodPost: UserCreatePage,
		},
		"/users/:id": {
			http.MethodGet: UserDetailPage,
		},
		"/users/:id/recovery": {
			http.MethodGet: UserRecoveryLinkPage,
		},
		"/users/:id/policies": {
			http.MethodGet: UserPoliciesPage,
		},
		"/users/:user/policies/:policy": {
			http.MethodGet:  PolicyAssignUserPage,
			http.MethodPost: PolicyAssignUser,
		},
		"/api-keys": {
			http.MethodGet:  APIKeyListPage,
			http.MethodPost: APIKeyListPage,
		},
		"/api-keys/:id/policies": {
			http.MethodGet: APIKeyPoliciesPage,
		},
		"/policies": {
			http.MethodGet: PoliciesListPage,
		},
		"/policies/create": {
			http.MethodGet:  PolicyCreatePage,
			http.MethodPost: PolicyCreate,
		},
		"/policies/:policy": {
			http.MethodGet: PolicyViewPage,
		},
		"/policies/:policy/edit": {
			http.MethodGet:  PolicyEditPage,
			http.MethodPost: PolicyEdit,
		},
		"/user-settings": {
			http.MethodGet: userSettings,
		},
	},
	Menu: &plugin_protos.MenuItem{
		Name: "IAM",
		Path: "/",
		Subitems: []*plugin_protos.MenuItem{
			{
				Name: "Users",
				Path: "/users",
				Subitems: []*plugin_protos.MenuItem{
					{Name: "List", Path: ""},
					{Name: "Create", Path: "/create"},
				},
			},
			{
				Name: "API Keys",
				Path: "/api-keys",
			},
			{
				Name: "Policies",
				Path: "/policies",
				Subitems: []*plugin_protos.MenuItem{
					{Name: "List", Path: ""},
					{Name: "Create", Path: "/create"},
				},
			},
		},
	},
}

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	p.ListenAndServe()
}

func getIAM(ctx context.Context) (*egiam.ClientWithResponses, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		logrus.Warn("error preparing iam client connection: ", err)
		return nil, apiConnectError
	}

	iam, err := egiam.NewClient(egClient)
	if err != nil {
		logrus.Warn("error preparing iam client connection: ", err)
		return nil, apiConnectError
	}

	return iam, nil
}
