// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"entanglement.garden/api-client/egiam"
	"entanglement.garden/iam/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	log "github.com/sirupsen/logrus"
)

func PoliciesListPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	responsePage := templates.Policies{}

	policies, err := iam.PermissionsListPoliciesWithResponse(ctx)
	if err != nil {
		log.Warn("error listing all policies from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting policy list"}
	}

	if policies.JSON200 == nil {
		log.Warn("non-successful response getting api key list: ", string(policies.Body))
		return nil, plugin.Error{Description: "unexpected error getting api key list"}
	}

	responsePage.Policies = *policies.JSON200

	return plugin.WritePage(ctx, request, &responsePage)
}

func PolicyViewPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	responsePage := templates.Policy{}

	policy, err := iam.PermissionsGetPolicyWithResponse(ctx, request.PathParams["policy"])
	if err != nil {
		log.Warn("error getting policy from IAM service: ", err)
		return nil, plugin.Error{Description: "error getting policy"}
	}

	if policy.JSON200 == nil {
		log.Warn("non-successful response getting policy: ", string(policy.Body))
		return nil, plugin.Error{Description: "unexpected error getting policy"}
	}

	responsePage.ID = *policy.JSON200.Id
	responsePage.Name = policy.JSON200.Name

	policyJSON, err := json.MarshalIndent(policy.JSON200, "", "    ")
	if err != nil {
		log.Warn("error marshalling policy json: ", err)
		return nil, plugin.Error{Description: "error rendering policy"}
	}

	responsePage.PolicyJSON = string(policyJSON)

	return plugin.WritePage(ctx, request, &responsePage)
}

func PolicyEditPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	responsePage := templates.PolicyEdit{}

	policy, err := iam.PermissionsGetPolicyWithResponse(ctx, request.PathParams["policy"])
	if err != nil {
		log.Warn("error getting policy from IAM service: ", err)
		return nil, plugin.Error{Description: "error getting policy"}
	}

	if policy.JSON200 == nil {
		log.Warn("non-successful response getting policy: ", string(policy.Body))
		return nil, plugin.Error{Description: "unexpected error getting policy"}
	}

	responsePage.ID = *policy.JSON200.Id
	responsePage.Name = policy.JSON200.Name

	policyJSON, err := json.MarshalIndent(policy.JSON200, "", "    ")
	if err != nil {
		log.Warn("error marshalling policy json: ", err)
		return nil, plugin.Error{Description: "error rendering policy"}
	}

	responsePage.PolicyJSON = string(policyJSON)
	responsePage.PolicyJSONLines = bytes.Count(policyJSON, []byte("\n"))

	return plugin.WritePage(ctx, request, &responsePage)
}

func PolicyEdit(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	policyID := request.PathParams["policy"]

	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	body, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, err
	}

	var policy egiam.Policy
	if err := json.Unmarshal([]byte(body.Get("policy")), &policy); err != nil {
		return nil, err
	}

	if *policy.Id != policyID {
		return nil, plugin.Error{Description: "request body had incorrect policy ID"}
	}

	resp, err := iam.PermissionsUpdatePolicy(ctx, policyID, policy)
	if err != nil {
		log.Warn("error updating policy: ", err)
		return nil, err
	}

	var redirect string
	if resp.StatusCode == http.StatusOK {
		redirect = fmt.Sprintf("/iam/policies/%s", policyID)
	} else {
		redirect = fmt.Sprintf("/iam/policies/%s/edit", policyID)
	}

	log.Debug("redirecting to ", redirect)

	return plugin.Redirect(redirect)
}

func PolicyCreatePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	return plugin.WritePage(ctx, request, &templates.PolicyCreatePage{})
}

func PolicyCreate(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	body, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, err
	}

	var policyRequest egiam.Policy

	if err := json.Unmarshal([]byte(body.Get("policy")), &policyRequest); err != nil {
		return nil, err
	}

	policy, err := iam.PermissionsCreatePolicyWithResponse(ctx, policyRequest)
	if err != nil {
		return nil, err
	}

	if policy.StatusCode() != http.StatusCreated {
		return nil, fmt.Errorf("error creating policy: %s", policy.Status())
	}

	return plugin.Redirect(fmt.Sprintf("/iam/policies/%s", *policy.JSON201.Id))
}
