// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"entanglement.garden/common/cluster"
	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/forms/formtemplates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	"entanglement.garden/webui/utils"
)

// Settings renders the user settings page
func userSettings(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	log := logging.GetLog(ctx)

	// flowID := c.QueryParam(queryParamFlow)
	path, err := url.Parse(request.Path)
	if err != nil {
		log.Warn("error parsing url for query params")
		return nil, err
	}

	flowID := path.Query().Get("flow")
	log = log.WithField("flow", flowID)

	userSettingsFullURL := fmt.Sprintf("https://console.%s/entanglement.garden/iam/user-settings", cluster.Domain)
	initiateAuthURL := fmt.Sprintf("https://kratos.%s/self-service/settings/browser", cluster.Domain)

	if flowID == "" {
		log.Debug("empty flow ID: redirecting to kratos to initiate auth flow")
		return plugin.Redirect(initiateAuthURL)
	}

	cookie := request.PathParams["cookie"] // magic path param added by webui for this route only

	log.Debug("asking kratos for user session")
	session, resp, err := config.C.Kratos(false).FrontendApi.ToSession(ctx).Cookie(cookie).Execute()
	if err != nil {
		// no session exists, initiate login flow
		if resp.StatusCode == http.StatusUnauthorized {
			u := url.URL{
				Scheme: "https",
				Host:   fmt.Sprintf("kratos.%s", cluster.Domain),
				Path:   "/self-service/login/browser",
			}
			u.Query().Set("return_to", userSettingsFullURL)

			return plugin.Redirect(u.String())
		}
		return nil, err
	}

	log.Debug("asking Kratos API for user settings page")
	flow, resp, err := config.C.Kratos(false).FrontendApi.GetSettingsFlow(ctx).Id(flowID).Cookie(cookie).Execute()
	if err != nil {
		if resp != nil {
			switch resp.StatusCode {
			case http.StatusGone:
				log.Debug("account settings flow expired")
				return plugin.Redirect(initiateAuthURL)
			case http.StatusUnauthorized:
				log.Debug("not authorized, redirecting to login")
				dest := url.URL{
					Scheme: "https",

					Path: "/login",
				}
				dest.Query().Add("r", userSettingsFullURL)
				return plugin.Redirect(dest.String())
			default:
				log.Warn("unexpected error from kratos: ", err)
				return nil, err
			}
		} else {
			log.Error("error communicating with Kratos: ", err)
			return nil, err
		}
	}

	ctx = utils.WithSession(ctx, session)

	return plugin.WritePage(ctx, request, &formtemplates.Settings{Flow: flow})
}
