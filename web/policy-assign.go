// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"entanglement.garden/api-client/egiam"
	"entanglement.garden/iam/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	log "github.com/sirupsen/logrus"
)

func PolicyAssignUserPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	userID := request.PathParams["user"]
	policyID := request.PathParams["policy"]

	user, err := iam.GetUserWithResponse(ctx, userID)
	if err != nil {
		log.Warn("error getting user from iam service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user"}
	}
	if user.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting user %s: %s", userID, user.Status())
	}

	page := &templates.PolicyAssignToUserPage{User: user.JSON200}

	policy, err := iam.PermissionsGetPolicyWithResponse(ctx, policyID)
	if err != nil {
		return nil, err
	}
	if policy.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting user policies %s: %s", userID, user.Status())
	}
	page.Policy = *policy.JSON200

	policies, err := iam.ListUserPoliciesWithResponse(ctx, userID)
	if err != nil {
		return nil, err
	}
	if policies.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting user policies %s: %s", userID, user.Status())
	}

	// check if this policy is currently assigned to this user
	for _, p := range *policies.JSON200 {
		if p == policyID {
			page.CurrentlyAssigned = true
			break
		}
	}

	return plugin.WritePage(ctx, request, page)
}

func PolicyAssignUser(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	userID := request.PathParams["user"]
	policy := []string{request.PathParams["policy"]}

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, err
	}

	change := egiam.PolicyAssignmentChange{}

	switch form.Get("action") {
	case "assign":
		change.Add = &policy
	case "unassign":
		change.Remove = &policy
	default:
		return nil, errors.New("unexpected action")
	}

	resp, err := iam.PermissionsChangeUserPoliciesWithResponse(ctx, userID, change)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode() != http.StatusNoContent {
		if resp.JSONDefault != nil {
			return nil, fmt.Errorf("%s attempting to update user policy: %s", resp.Status(), resp.JSONDefault.Message)
		}

		return nil, fmt.Errorf("%s attempting to update user policy", resp.Status())
	}

	log.Debugf("change user policy response: %+v", resp.HTTPResponse)

	return plugin.Redirect(fmt.Sprintf("/iam/users/%s/policies/%s", userID, policy[0]))
}

func UserPoliciesPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	userID := request.PathParams["id"]

	page := templates.PolicyListForUserPage{}
	user, err := iam.GetUserWithResponse(ctx, userID)
	if err != nil {
		log.Warn("error getting user from iam service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting user"}
	}
	if user.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting user %s: %s", userID, user.Status())
	}
	page.User = user.JSON200

	allPolicies, err := iam.PermissionsListPoliciesWithResponse(ctx)
	if err != nil {
		return nil, err
	}
	if allPolicies.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting policy list: %s", allPolicies.Status())
	}
	page.AllPolicies = *allPolicies.JSON200

	page.SelectedPolicies = map[string]bool{}
	policies, err := iam.ListUserPoliciesWithResponse(ctx, userID)
	if err != nil {
		return nil, err
	}
	if policies.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting user policies %s: %s", userID, policies.Status())
	}
	for _, policy := range *policies.JSON200 {
		page.SelectedPolicies[policy] = true
	}

	return plugin.WritePage(ctx, request, &page)
}

func APIKeyPoliciesPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	apiKey := request.PathParams["id"]

	page := templates.PolicyListForUserPage{}
	apikey, err := iam.GetApiKeyWithResponse(ctx, apiKey)
	if err != nil {
		log.Warn("error getting api key from iam service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting api key"}
	}
	if apikey.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting apikey %s: %s", apiKey, apikey.Status())
	}

	allPolicies, err := iam.PermissionsListPoliciesWithResponse(ctx)
	if err != nil {
		return nil, err
	}
	if allPolicies.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting policy list: %s", allPolicies.Status())
	}
	page.AllPolicies = *allPolicies.JSON200

	page.SelectedPolicies = map[string]bool{}
	policies, err := iam.ListUserPoliciesWithResponse(ctx, apiKey)
	if err != nil {
		return nil, err
	}
	if policies.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("error getting api key policies %s: %s", apiKey, policies.Status())
	}
	for _, policy := range *policies.JSON200 {
		page.SelectedPolicies[policy] = true
	}

	return plugin.WritePage(ctx, request, &page)
}
