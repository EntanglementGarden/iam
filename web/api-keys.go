// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"net/http"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/iam/web/templates"

	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func APIKeyListPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	iam, err := getIAM(ctx)
	if err != nil {
		return nil, err
	}

	responsePage := templates.APIKeys{}

	if request.Method == http.MethodPost {
		newKey, err := iam.CreateApiKeyWithResponse(ctx)
		if err != nil {
			log.Warn("error from IAM service while creating API key: ", err)
			return nil, plugin.Error{Description: "unexpected error creating API key"}
		}

		if newKey.JSON201 == nil {
			log.Warn("non-successful response creating api key: ", string(newKey.Body))
			return nil, plugin.Error{Description: "unexpected error creating API key"}
		}
		responsePage.NewKey = newKey.JSON201
	}

	apiKeys, err := iam.ListApiKeysWithResponse(ctx)
	if err != nil {
		log.Warn("error listing all api keys from IAM service: ", err)
		return nil, plugin.Error{Description: "unexpected error getting api key list"}
	}

	if apiKeys.JSON200 == nil {
		log.Warn("non-successful response getting api key list: ", string(apiKeys.Body))
		return nil, plugin.Error{Description: "unexpected error getting api key list"}
	}

	responsePage.Keys = *apiKeys.JSON200

	return plugin.WritePage(ctx, request, &responsePage)
}
