package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	kratos "github.com/ory/kratos-client-go"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/common/installdata"
	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/kratosx"
)

func bootstrap() error {
	ctx, log := logging.LogWithField(context.Background(), "component", "iam-bootstrap")

	if err := bootstrapUsers(logging.WithField(ctx, "bootstrap", "users")); err != nil {
		log.Error("error creating initial superuser")
		return err
	}

	if err := bootstrapAPIKeys(logging.WithField(ctx, "bootstrap", "apikeys")); err != nil {
		log.Error("error creating initial superuser api key")
		return err
	}

	return nil
}

func bootstrapAPIKeys(ctx context.Context) error {
	log := logging.GetLog(ctx)

	log.Info("reading pre-defined superuser API key from disk")
	authString, err := egapi.GetInternalAuthString()
	if err != nil {
		return err
	}

	parsedAuth, err := url.Parse(authString)
	if err != nil {
		return fmt.Errorf("error parsing auth string: %v", err)
	}

	key := parsedAuth.Query().Get("key")
	secret := parsedAuth.Query().Get("secret")

	log.Info("initializing database")
	if err := db.Migrate(); err != nil {
		return err
	}

	// load superuser api key into kratos
	log.Info("adding API key to Kratos")
	identity, err := kratosx.AddApiKey(ctx, key, secret)
	if err != nil {
		return err
	}

	// mark superuser api key as a superuser internally
	log.WithField("identity", identity).Info("marking key as superuser in database")
	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	if err := queries.SuperuserAdd(ctx, identity); err != nil {
		return err
	}

	return nil
}

func bootstrapUsers(ctx context.Context) error {
	log := logging.GetLog(ctx)

	log.Debug("reading install data")
	installData, err := installdata.ReadInstallData()
	if err != nil {
		return err
	}

	traits := map[string]interface{}{"username": installData.SuperuserName}

	if installData.SuperuserEmail != "" {
		traits["email"] = installData.SuperuserEmail
	}

	log.WithField("traits", traits).Info("creating superuser")

	k := config.C.Kratos(true)
	identity, resp, err := k.IdentityApi.CreateIdentity(ctx).CreateIdentityBody(kratos.CreateIdentityBody{
		SchemaId: "person",
		Traits:   traits,
		Credentials: &kratos.IdentityWithCredentials{
			Password: &kratos.IdentityWithCredentialsPassword{
				Config: &kratos.IdentityWithCredentialsPasswordConfig{
					HashedPassword: &installData.SuperuserHashedPassword,
				},
			},
		},
	}).Execute()
	if err != nil && resp != nil && resp.StatusCode == http.StatusConflict {
		log.Info("user by that name already exists (received HTTP 409), continuing")
	} else if err != nil {
		log.Debug("error from Kratos while creating identity")
		return err
	}

	// mark user as a superuser internally
	log.WithField("user_id", identity.Id).Info("marking user as superuser in database")
	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	if err := queries.SuperuserAdd(ctx, identity.Id); err != nil {
		return err
	}

	log.Info("created first superuser")

	return nil
}
