// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"

	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/forms"
	"entanglement.garden/iam/httpserver"
	"entanglement.garden/iam/ketox"
)

var signals = make(chan os.Signal, 1)

func main() {
	config.Load()
	logrus.SetLevel(logrus.TraceLevel)

	if len(os.Args) > 1 && os.Args[1] == "initialize" {
		if err := bootstrap(); err != nil {
			logging.GetLog(context.Background()).Fatal("error bootstrapping iam: ", err)
		}

		return
	}

	if err := db.Migrate(); err != nil {
		logrus.Fatal("error migrating database: ", err)
	}

	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)

	ctx, cancel := context.WithCancel(context.Background())
	go httpserver.Serve()
	go forms.Serve()
	go ketox.EnsureSuperuser(ctx)

	for {
		signal := <-signals
		logrus.Debug("Received signal", signal)
		switch signal {
		case syscall.SIGINT:
			httpserver.Shutdown(context.Background())
			forms.Shutdown(context.Background())
			cancel()
			return
		}
	}
}
