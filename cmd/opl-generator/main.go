// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"os"

	"entanglement.garden/iam/ketox"
	"entanglement.garden/iam/oplbuilder"
	"github.com/sirupsen/logrus"
)

func main() {
	err := oplbuilder.Render(os.Stdout, ketox.AllExtensions...)
	if err != nil {
		logrus.Fatal(err)
	}
}
