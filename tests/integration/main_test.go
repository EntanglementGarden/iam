// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package integration_test

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	client "github.com/ory/keto-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egiam"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/httpserver"
	"entanglement.garden/iam/ketox"
)

const (
	apiServerBind = "127.0.0.1:4468"
)

var (
	testToken string

	superuserUserID = uuid.Must(uuid.NewRandom()).String()

	instanceID = "entanglement.garden:rhyzome:instance:" + identifiers.GenerateID(identifiers.PrefixInstance)
)

func TestMain(m *testing.M) {
	logrus.SetLevel(logrus.TraceLevel)

	initializeJWT()

	ketoReadServer := os.Getenv("KETO_PORT_4466_TCP_ADDR")
	ketoWriteServer := os.Getenv("KETO_PORT_4467_TCP_ADDR")
	if ketoReadServer == "" || ketoWriteServer == "" {
		fmt.Println("skipping keto integration tests because keto server IP is not set")
		os.Exit(0)
	}

	config.C.KetoReadServer = client.ServerConfigurations{{URL: fmt.Sprintf("http://%s:4466", ketoReadServer)}}
	config.C.KetoWriteServer = client.ServerConfigurations{{URL: fmt.Sprintf("http://%s:4467", ketoWriteServer)}}
	config.C.APIServerBind = apiServerBind

	ketox.EnsureSuperuser(context.Background())

	// egapi.AddInternalHost("iam", apiServerBind)

	tmpdir, err := os.MkdirTemp("", "entanglement-iam-test")
	if err != nil {
		panic(err)
	}
	defer func() {
		os.RemoveAll(tmpdir)
	}()

	config.C.SqlitePath = fmt.Sprintf("%s/iam.db", tmpdir)

	if err := db.Migrate(); err != nil {
		panic(err)
	}

	go httpserver.Serve()

	for {
		_, err := http.Get(fmt.Sprintf("http://%s/health", apiServerBind))
		if err != nil {
			logrus.Debug("waiting for server to start: ", err)
			time.Sleep(time.Microsecond * 50)
		} else {
			break
		}
	}

	code := m.Run()

	httpserver.Shutdown(context.Background())
	os.Exit(code)
}

// generate a JWT signing key and use it to sign a JWT, which will be used for API calls during this test
func initializeJWT() {
	jwkPrivateKey, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		panic(err)
	}

	httpx.JwtX = jwkPrivateKey.X.Bytes()
	httpx.JwtY = jwkPrivateKey.Y.Bytes()

	testToken, err = jwt.NewWithClaims(jwt.SigningMethodES512, httpx.JWTClaims{
		RegisteredClaims: jwt.RegisteredClaims{Subject: superuserUserID},
		Username:         superuserUserID,
	}).SignedString(jwkPrivateKey)
	if err != nil {
		panic(err)
	}
}

func getAPIclient() *egiam.ClientWithResponses {
	eg, err := egapi.NewClient(egapi.InternalAuth)
	if err != nil {
		panic(err)
	}

	eg.InternalJWT = testToken

	iam, err := egiam.NewClient(eg)
	if err != nil {
		panic(err)
	}

	return iam
}
