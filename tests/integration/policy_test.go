// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package integration_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"entanglement.garden/api-client/egiam"
)

// i wrote this and then realized i could directly call into ketox package,
// but this is a good sample http test
func TestPolicyUpsertHTTP(t *testing.T) {
	ctx := context.Background()

	egapi := getAPIclient()

	policy := egiam.Policy{
		Name:        "test policy",
		Objects:     []string{"entanglement.garden:rhyzome:instance:*"},
		Permissions: []string{"create", "read", "list"},
	}

	createResp, err := egapi.PermissionsCreatePolicyWithResponse(ctx, policy)
	assert.NoError(t, err, "create policy")
	assert.Equal(t, http.StatusCreated, createResp.StatusCode())
	assert.NotNil(t, createResp.JSON201)
	assert.NotNil(t, createResp.JSON201.Id)
	assert.Equal(t, policy.Name, createResp.JSON201.Name)
	for _, o := range policy.Objects {
		assert.Contains(t, createResp.JSON201.Objects, o)
	}
	for _, r := range policy.Permissions {
		assert.Contains(t, createResp.JSON201.Permissions, r)
	}

	policyID := createResp.JSON201.Id

	newPolicy := egiam.Policy{
		Name:        "testier policist",
		Objects:     []string{instanceID},
		Permissions: []string{"create", "update", "delete"},
	}

	updateResp, err := egapi.PermissionsUpdatePolicyWithResponse(ctx, *policyID, newPolicy)
	assert.NoError(t, err, "update policy")
	assert.Equal(t, http.StatusOK, updateResp.StatusCode())
	assert.NotNil(t, updateResp.JSON200)

	getResp, err := egapi.PermissionsGetPolicyWithResponse(ctx, *policyID)
	assert.NoError(t, err, "get policy")
	assert.Equal(t, http.StatusOK, getResp.StatusCode())
	assert.NotNil(t, getResp.JSON200)

	assert.Equal(t, newPolicy.Name, getResp.JSON200.Name)
	for _, o := range newPolicy.Objects {
		assert.Contains(t, getResp.JSON200.Objects, o)
	}
	for _, r := range newPolicy.Permissions {
		assert.Contains(t, getResp.JSON200.Permissions, r)
	}
}
