// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package forms

import (
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/iam/config"
	"entanglement.garden/iam/forms/formtemplates"
	"entanglement.garden/webui/templates"
)

// AccountRecovery begins the user account recovery (password reset) flow
func AccountRecovery(c echo.Context) error {
	flowID := c.QueryParam(queryParamFlow)

	if flowID == "" {
		return initiateAuthProcess(c)
	}

	cookie := c.Request().Header.Get("Cookie")
	flow, resp, err := config.C.Kratos(false).FrontendApi.GetRecoveryFlow(c.Request().Context()).Id(flowID).Cookie(cookie).Execute()
	if err != nil {
		if resp != nil {
			switch resp.StatusCode {
			case http.StatusGone:
				log.Debug("account recovery flow expired")
				return initiateAuthProcess(c)
			default:
				log.Warn("unexpected error from kratos: ", err)
				return initiateAuthProcess(c)
			}
		} else {
			log.Error("error communicating with Kratos: ", err)
			return writeTemplate(c, &templates.Error{Description: "error communicating with Kratos. more details may be found in the server logs"})
		}
	}

	return writeTemplate(c, &formtemplates.AccountRecovery{Flow: flow})
}
