// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package forms

import (
	"context"
	_ "embed"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/cluster"
	"entanglement.garden/common/httpx"
	"entanglement.garden/iam/config"
	"entanglement.garden/webui/static"
)

var (
	e *echo.Echo
)

// Serve starts the HTTP listener
func Serve() {
	e = httpx.NewUnauthenticatedServer()

	e.GET("/", func(c echo.Context) error {
		return c.Redirect(http.StatusFound, fmt.Sprintf("https://console.%s/entanglement.garden/iam/user-settings", cluster.Domain))
	})
	e.GET("/login", Login)
	e.GET("/logout", Logout)
	e.GET("/recovery", AccountRecovery)

	e.StaticFS("/static", static.FS)

	log.Info("starting form server on ", config.C.FormServerBind)
	err := e.Start(config.C.FormServerBind)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

// Shutdown gracefully stops the HTTP listener
func Shutdown(ctx context.Context) {
	if e != nil {
		log.Info("shutting down form server")
		err := e.Shutdown(ctx)
		if err != nil {
			log.Error("error shutting down form server: ", err)
		}
	}
}
