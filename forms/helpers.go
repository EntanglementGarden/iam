// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package forms

import (
	"fmt"
	"net/http"
	"net/url"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/common/cluster"
	"entanglement.garden/common/logging"
	"entanglement.garden/webui/templates"
)

// legacy mechanism to start a flow, only starts the login flow and often misused
func initiateAuthProcess(c echo.Context) error {
	return c.Redirect(http.StatusFound, fmt.Sprintf("https://kratos.%s/self-service/login/browser", cluster.Domain))
}

func writeTemplate(c echo.Context, page templates.Page) error {
	templates.WritePageTemplate(c.Response(), c.Request().Context(), c.Request().URL.Path, page)
	return nil
}

func initiateFlow(c echo.Context, flow string) error {
	destURL := fmt.Sprintf("https://kratos.%s/self-service/%s/browser", cluster.Domain, flow)

	if flow == "login" {
		q := url.Values{"return_to": []string{c.Request().RequestURI}}
		destURL = destURL + "?" + q.Encode()
	}

	logging.GetLog(c.Request().Context()).WithField("flow", flow).Debug("initiating flow")
	return c.Redirect(http.StatusFound, destURL)
}
