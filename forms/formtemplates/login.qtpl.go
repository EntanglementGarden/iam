// Code generated by qtc from "login.qtpl". DO NOT EDIT.
// See https://github.com/valyala/quicktemplate for details.

// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only
//

//line forms/formtemplates/login.qtpl:4
package formtemplates

//line forms/formtemplates/login.qtpl:4
import kratos "github.com/ory/kratos-client-go"

//line forms/formtemplates/login.qtpl:6
import (
	qtio422016 "io"

	qt422016 "github.com/valyala/quicktemplate"
)

//line forms/formtemplates/login.qtpl:6
var (
	_ = qtio422016.Copy
	_ = qt422016.AcquireByteBuffer
)

//line forms/formtemplates/login.qtpl:7
type Login struct {
	Flow        *kratos.LoginFlow
	Destination string
}

//line forms/formtemplates/login.qtpl:13
func (p *Login) StreamTitle(qw422016 *qt422016.Writer) {
//line forms/formtemplates/login.qtpl:13
	qw422016.N().S(`Login`)
//line forms/formtemplates/login.qtpl:13
}

//line forms/formtemplates/login.qtpl:13
func (p *Login) WriteTitle(qq422016 qtio422016.Writer) {
//line forms/formtemplates/login.qtpl:13
	qw422016 := qt422016.AcquireWriter(qq422016)
//line forms/formtemplates/login.qtpl:13
	p.StreamTitle(qw422016)
//line forms/formtemplates/login.qtpl:13
	qt422016.ReleaseWriter(qw422016)
//line forms/formtemplates/login.qtpl:13
}

//line forms/formtemplates/login.qtpl:13
func (p *Login) Title() string {
//line forms/formtemplates/login.qtpl:13
	qb422016 := qt422016.AcquireByteBuffer()
//line forms/formtemplates/login.qtpl:13
	p.WriteTitle(qb422016)
//line forms/formtemplates/login.qtpl:13
	qs422016 := string(qb422016.B)
//line forms/formtemplates/login.qtpl:13
	qt422016.ReleaseByteBuffer(qb422016)
//line forms/formtemplates/login.qtpl:13
	return qs422016
//line forms/formtemplates/login.qtpl:13
}

//line forms/formtemplates/login.qtpl:15
func (p *Login) StreamBody(qw422016 *qt422016.Writer) {
//line forms/formtemplates/login.qtpl:15
	qw422016.N().S(`
<span class="login">
`)
//line forms/formtemplates/login.qtpl:17
	streamrenderKratosUI(qw422016, p.Flow.Ui)
//line forms/formtemplates/login.qtpl:17
	qw422016.N().S(`
</span>
`)
//line forms/formtemplates/login.qtpl:19
}

//line forms/formtemplates/login.qtpl:19
func (p *Login) WriteBody(qq422016 qtio422016.Writer) {
//line forms/formtemplates/login.qtpl:19
	qw422016 := qt422016.AcquireWriter(qq422016)
//line forms/formtemplates/login.qtpl:19
	p.StreamBody(qw422016)
//line forms/formtemplates/login.qtpl:19
	qt422016.ReleaseWriter(qw422016)
//line forms/formtemplates/login.qtpl:19
}

//line forms/formtemplates/login.qtpl:19
func (p *Login) Body() string {
//line forms/formtemplates/login.qtpl:19
	qb422016 := qt422016.AcquireByteBuffer()
//line forms/formtemplates/login.qtpl:19
	p.WriteBody(qb422016)
//line forms/formtemplates/login.qtpl:19
	qs422016 := string(qb422016.B)
//line forms/formtemplates/login.qtpl:19
	qt422016.ReleaseByteBuffer(qb422016)
//line forms/formtemplates/login.qtpl:19
	return qs422016
//line forms/formtemplates/login.qtpl:19
}
