// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package forms

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/cluster"
	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/forms/formtemplates"
	"entanglement.garden/webui/templates"
)

const (
	queryParamFlow     = "flow"
	queryParamRedirect = "r"
)

// Login begins the user login flow
func Login(c echo.Context) error {
	log := logging.GetLog(c.Request().Context())
	log.Trace("rendering login form")

	cookie := c.Request().Header.Get("Cookie")
	ctx := c.Request().Context()
	flowID := c.QueryParam(queryParamFlow)

	if flowID == "" {
		log.Trace("flow ID is blank, checking session")
		// check if there's an active session
		session, resp, err := config.C.Kratos(false).FrontendApi.ToSession(ctx).Cookie(cookie).Execute()
		if err != nil {
			log.WithField("err", err).Debug("error checking session in kratos")
			// no session exists, initiate login flow
			if resp != nil && resp.StatusCode == http.StatusUnauthorized {
				return initiateFlow(c, "login")
			}
			return err
		}

		if session != nil {
			redirect := c.QueryParam(queryParamRedirect)
			if redirect == "" {
				redirect = fmt.Sprintf("https://console.%s", cluster.Domain)
			}
			log.WithField("target", redirect).Debug("session exists, redirecting")
			return c.Redirect(http.StatusFound, redirect)
		}

		return initiateFlow(c, "login")
	}

	flow, resp, err := config.C.Kratos(false).FrontendApi.CreateBrowserLoginFlow(ctx).Cookie(cookie).Execute()
	if err != nil {
		if resp != nil {
			switch resp.StatusCode {
			case http.StatusGone:
				log.Debug("login flow expired, restarting login")
				return initiateFlow(c, "login")
			default:
				log.WithField("err", err).WithField("status", resp.Status).Warn("unexpected error calling kratos")
				return initiateFlow(c, "login")
			}
		} else {
			log.Error("error communicating with Kratos: ", err)
			return writeTemplate(c, &templates.Error{Description: "error communicating with Kratos. more details may be found in the server logs"})
		}
	}

	log.Trace("writing login template")
	return writeTemplate(c, &formtemplates.Login{Flow: flow})
}

// Logout asks Kratos for a logout URL for the current session, then redirects to it
func Logout(c echo.Context) error {
	cookie := c.Request().Header.Get("Cookie")
	logoutURL, _, err := config.C.Kratos(false).FrontendApi.CreateBrowserLogoutFlow(c.Request().Context()).Cookie(cookie).Execute()
	if err != nil {
		logrus.Warn("error getting logout URL from kratos: ", err)
		return writeTemplate(c, &templates.Error{Error: err})
	}

	return c.Redirect(http.StatusFound, logoutURL.LogoutUrl)
}
