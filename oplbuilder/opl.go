// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package oplbuilder

import (
	"fmt"
	"io"
	"text/template"

	"entanglement.garden/iam/ketox"
)

const oplTemplate = `import { Namespace, SubjectSet, Context } from "@ory/keto-namespace-types"

class User implements Namespace {}

class Group implements Namespace {
  related: {
    members: (User | Group)[]
  }
}

class Policy implements Namespace {
  related: {
    members: (User | Group)[]
  }
}
{{ range . }}
// {{ .EntanglementName }}
class {{ .Name }} implements Namespace {
  related: {{"{"}}{{ range .Relations }}
    {{ . }}: SubjectSet<Policy, "members">[],{{ end }}
    parent: {{ .Name }}[],
  }

  permits = {{"{"}}{{ range .Relations }}
    {{ . }}: (ctx: Context): boolean => this.related.parent.traverse((p) => p.permits.{{ . }}(ctx)) || this.related.{{ . }}.includes(ctx.subject),{{ end }}
  }
}{{ end }}`

type permissionNamespace struct {
	EntanglementName string
	Name             string
	Relations        []string
}

func Render(w io.Writer, extensions ...ketox.Extension) error {
	t := template.Must(template.New("").Parse(oplTemplate))

	namespaces := []permissionNamespace{}
	for _, extension := range extensions {
		for _, object := range extension.Objects {
			objectID := fmt.Sprintf("%s:%s:%s:", extension.Domain, extension.Slug, object.Name)
			namespace, _, err := ketox.ParseEntanglementObjectID(objectID)
			if err != nil {
				return err
			}

			permissions := []string{}
			for _, p := range object.Permissions {
				permissions = append(permissions, ketox.ParseEntanglementIdentifier(p))
			}

			namespaces = append(namespaces, permissionNamespace{
				EntanglementName: objectID,
				Name:             namespace,
				Relations:        permissions,
			})
		}
	}

	if err := t.Execute(w, namespaces); err != nil {
		return err
	}

	return nil
}
