// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"errors"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/ketox"
	"entanglement.garden/iam/openapi"
)

func (server) ListUserPolicies(c echo.Context, user string) error {
	ctx := c.Request().Context()

	// verify that the user exists
	_, _, err := config.C.Kratos(true).IdentityApi.GetIdentity(ctx, user).Execute()
	if err != nil {
		return err
	}

	policies, err := ketox.GetUserPolicies(ctx, user)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, policies)
}

func (server) PermissionsChangeUserPolicies(c echo.Context, user string) error {
	log := logging.GetLog(c.Request().Context())

	var req openapi.PolicyAssignmentChange
	if err := c.Bind(&req); err != nil {
		return err
	}

	ctx := c.Request().Context()

	// verify that the user exists
	_, _, err := config.C.Kratos(true).IdentityApi.GetIdentity(ctx, user).Execute()
	if err != nil {
		return err
	}

	var add []string
	if req.Add != nil {
		add = *req.Add
		log.Debug("adding user policies: ", add)
	}

	var remove []string
	if req.Remove != nil {
		remove = *req.Remove
		log.Debug("removing user policies: ", remove)
	}

	err = ketox.ChangeUserPolicies(c, user, add, remove)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) ListApiKeyPolicies(c echo.Context, apikey string) error {
	ctx := c.Request().Context()

	// verify that the api key exists
	_, _, err := config.C.Kratos(true).IdentityApi.GetIdentity(ctx, apikey).Execute()
	if err != nil {
		return err
	}

	policies, err := ketox.GetUserPolicies(ctx, apikey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, policies)
}

func (server) PermissionsChangeApiKeyPolicies(c echo.Context, apikey string) error {
	var req openapi.PolicyAssignmentChange
	if err := c.Bind(&req); err != nil {
		return err
	}

	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	identities, _, err := config.C.Kratos(true).IdentityApi.ListIdentities(ctx).CredentialsIdentifier(apikey).Execute()
	if err != nil {
		return err
	}

	if len(identities) != 1 {
		return errors.New("API key not found") // http 404
	}

	if len(identities) > 1 {
		return fmt.Errorf("attempted to get Kratos UUID for API key, got %d results from kratos", len(identities)) // http 500
	}

	identity := identities[0]

	log.WithField("api_key_uuid", identity.Id).Debug("got api key UUID from kratos")

	var add []string
	if req.Add != nil {
		add = *req.Add
	}

	var remove []string
	if req.Remove != nil {
		remove = *req.Remove
	}

	err = ketox.ChangeUserPolicies(c, identity.Id, add, remove)
	if err != nil {
		return fmt.Errorf("error changing API key policies: %v", err)
	}

	return c.NoContent(http.StatusNoContent)
}
