// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"fmt"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/openapi"
)

func (server) Whoami(c echo.Context) error {
	ctx := c.Request().Context()
	claims, err := httpx.GetJWTClaims(ctx)
	if err != nil {
		return err
	}

	log.Debugf("whoami claims: %+v", claims)

	k := config.C.Kratos(true)
	identity, _, err := k.IdentityApi.GetIdentity(ctx, claims.Subject).Execute()
	if err != nil {
		return err
	}

	log.Debugf("request was made by identity: %+v", identity)

	apikey, err := identityToAPIKey(*identity)
	if err != nil {
		return fmt.Errorf("error parsing requester identity as API key: %v", err)
	}

	return c.JSON(200, openapi.APIKey{
		ApiKey: apikey.ApiKey,
	})
}
