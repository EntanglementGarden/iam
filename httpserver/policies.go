// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"errors"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"
	client "github.com/ory/keto-client-go"

	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/common/logging"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/db"
	"entanglement.garden/iam/ketox"
	"entanglement.garden/iam/openapi"
)

func (server) PermissionsListPolicies(c echo.Context) error {
	ctx := c.Request().Context()
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	policies := []openapi.Policy{}

	dbPolicies, err := queries.GetPoliciesByTenant(ctx, db.NullableString(identifiers.DefaultTenant))
	if err != nil {
		return err
	}

	for _, p := range dbPolicies {
		policies = append(policies, openapi.Policy{
			Id:   &p.ID,
			Name: p.Name,
		})
	}

	return c.JSON(http.StatusOK, policies)
}

func (server) PermissionsCreatePolicy(c echo.Context) error {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	var req openapi.Policy
	if err := c.Bind(&req); err != nil {
		return err
	}

	params := db.UpsertPolicyParams{
		ID:     identifiers.GenerateID(identifiers.PrefixPolicy),
		Name:   req.Name,
		Tenant: db.NullableString(identifiers.DefaultTenant),
	}

	req.Id = &params.ID

	requester, err := httpx.GetJWTClaims(ctx)
	if err != nil {
		return fmt.Errorf("error getting requesting user: %v", err)
	}

	if err := ketox.UpsertPolicy(ctx, params, req.Permissions, req.Objects, requester.Subject); err != nil {
		genericErr := &client.GenericOpenAPIError{}
		if errors.As(err, genericErr) {
			log.Debugf("generic error: %+v", genericErr)
		}

		log.Errorf("error upserting policy: %+v", err)
		return err
	}

	return c.JSON(http.StatusCreated, req)
}

func (server) PermissionsDeletePolicy(c echo.Context, policyID string) error {
	ctx := c.Request().Context()

	if err := ketox.DeletePolicy(ctx, policyID); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) PermissionsGetPolicy(c echo.Context, policyID string) error {
	policy, relationships, objects, err := ketox.GetPolicy(c.Request().Context(), policyID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, openapi.Policy{
		Id:          &policy.ID,
		Name:        policy.Name,
		Objects:     objects,
		Permissions: relationships,
	})
}

func (server) PermissionsUpdatePolicy(c echo.Context, policyID string) error {
	ctx := c.Request().Context()
	log := logging.GetLog(ctx)

	var req openapi.Policy
	if err := c.Bind(&req); err != nil {
		return err
	}

	params := db.UpsertPolicyParams{
		ID:     policyID,
		Name:   req.Name,
		Tenant: db.NullableString(identifiers.DefaultTenant),
	}

	req.Id = &params.ID
	requester, err := httpx.GetJWTClaims(ctx)
	if err != nil {
		return fmt.Errorf("error getting requesting user: %v", err)
	}

	log.Debugf("upserting policy: %+v", req)
	if err := ketox.UpsertPolicy(ctx, params, req.Permissions, req.Objects, requester.Subject); err != nil {
		genericErr := &client.GenericOpenAPIError{}
		if errors.As(err, genericErr) {
			log.Debugf("generic error: %+v", genericErr)
		}

		log.Errorf("error upserting policy: %+v", err)
		return err
	}

	return c.JSON(http.StatusOK, req)
}

func (server) AddObject(c echo.Context, objectID string) error {
	if err := ketox.AddDeleteObject(c.Request().Context(), objectID, true); err != nil {
		return err
	}

	return nil
}

func (server) DeleteObject(c echo.Context, objectID string) error {
	if err := ketox.AddDeleteObject(c.Request().Context(), objectID, false); err != nil {
		return err
	}

	return nil
}

func (server) PermissionsExpand(c echo.Context, objectID string, permission string) error {
	namespace, object, err := ketox.ParseEntanglementObjectID(objectID)
	if err != nil {
		return httpx.HTTPResponseError{
			Err:    err,
			Status: http.StatusBadRequest,
		}
	}

	r := config.C.Keto(false).PermissionApi.ExpandPermissions(c.Request().Context())
	r = r.Namespace(namespace)
	r = r.Object(object)
	r = r.Relation(permission)
	tree, _, err := r.Execute()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, tree)
}
