// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"context"
	_ "embed"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/iam/config"
	"entanglement.garden/iam/openapi"
)

var (
	e *echo.Echo

	//go:embed web.ewp
	ewp []byte
)

type server struct{}

// Serve starts the HTTP listener
func Serve() {
	e = httpx.NewServerWithEWP(ewp)
	openapi.RegisterHandlersWithBaseURL(e, &server{}, "/entanglement.garden/iam")

	log.Info("starting API server on ", config.C.APIServerBind)
	err := e.Start(config.C.APIServerBind)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

// Shutdown gracefully stops the HTTP listener
func Shutdown(ctx context.Context) {
	if e != nil {
		log.Info("shutting down api server")
		err := e.Shutdown(ctx)
		if err != nil {
			log.Error("error shutting down api server: ", err)
		}
	}
}

// func handleError(err error, c echo.Context) {
// 	code := http.StatusInternalServerError

// 	if kratosErr, ok := err.(*kratos.GenericOpenAPIError); ok { // this works
// 		log.Debugf("generic openapi error: %v", kratosErr.Error())

// 		if genericErr, ok := kratosErr.Model().(*kratos.GenericError); ok { // this doesn't work
// 			c.JSON(code, map[string]interface{}{
// 				"code":    genericErr.Code,
// 				"details": genericErr.Details,
// 				"message": genericErr.Message,
// 				"reason":  genericErr.Reason,
// 				"status":  genericErr.Status,
// 			})
// 			return
// 		}
// 	}

// 	if he, ok := err.(*echo.HTTPError); ok {
// 		code = he.Code
// 	}

// 	log.Error(err)
// 	c.JSON(code, map[string]string{"error": err.Error()})
// }
