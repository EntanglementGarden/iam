// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"errors"
	"net/http"

	echo "github.com/labstack/echo/v4"
	kratos "github.com/ory/kratos-client-go"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/iam/config"
	"entanglement.garden/iam/openapi"
)

const schemaIDPerson = "person"

func (server) CreateUser(c echo.Context) error {
	log.Debug("creating user")

	var req openapi.UserCreateRequest
	if err := c.Bind(&req); err != nil {
		return err
	}

	ctx := c.Request().Context()

	traits := map[string]interface{}{"username": req.Username}

	if req.Email != nil {
		traits["email"] = &req.Email
	}

	k := config.C.Kratos(true)
	identity, _, err := k.IdentityApi.CreateIdentity(ctx).CreateIdentityBody(kratos.CreateIdentityBody{
		SchemaId: schemaIDPerson,
		Traits:   traits,
	}).Execute()
	if err != nil {
		log.Debug("error from Kratos while creating identity")
		return err
	}

	log.Debug("user created")
	return c.JSON(http.StatusCreated, openapi.UserCreateResponse{Id: identity.Id})
}

func (server) ListAllUsers(c echo.Context) error {
	ctx := c.Request().Context()

	k := config.C.Kratos(true)
	identities, _, err := k.IdentityApi.ListIdentities(ctx).Execute()
	if err != nil {
		return err
	}

	response := []openapi.User{}
	for _, identity := range identities {
		if identity.SchemaId != schemaIDPerson {
			continue
		}

		user, err := identityToUser(identity)
		if err != nil {
			return err
		}

		response = append(response, user)
	}

	return c.JSON(http.StatusOK, response)
}

func (server) GetUser(c echo.Context, id string) error {
	ctx := c.Request().Context()

	k := config.C.Kratos(true)
	identity, _, err := k.IdentityApi.GetIdentity(ctx, id).Execute()
	if err != nil {
		return err
	}

	user, err := identityToUser(*identity)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, user)
}

func (server) GenerateRecoveryLink(c echo.Context, id string) error {
	ctx := c.Request().Context()

	k := config.C.Kratos(true)
	link, _, err := k.IdentityApi.CreateRecoveryCodeForIdentity(ctx).CreateRecoveryCodeForIdentityBody(kratos.CreateRecoveryCodeForIdentityBody{
		IdentityId: id,
	}).Execute()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, openapi.UserRecoveryLink{
		RecoveryLink: link.RecoveryLink,
		ExpiresAt:    link.ExpiresAt,
	})

}

func identityToUser(identity kratos.Identity) (openapi.User, error) {
	traits, ok := identity.Traits.(map[string]interface{})
	if !ok {
		log.Debug("person: %+V", identity.Traits)

		return openapi.User{}, errors.New("unable to parse traits as person! this should never happen")
	}

	user := openapi.User{Id: identity.GetId()}

	if username := traits["username"]; username != nil {
		user.Username = username.(string)
	}

	if email := traits["email"]; email != nil {
		user.Email = email.(string)
	}

	return user, nil
}
