// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"net/http"

	echo "github.com/labstack/echo/v4"
	kratos "github.com/ory/kratos-client-go"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/iam/config"
	"entanglement.garden/iam/kratosx"
	"entanglement.garden/iam/openapi"
)

func (server) ListApiKeys(c echo.Context) error {
	ctx := c.Request().Context()

	k := config.C.Kratos(true)
	identities, _, err := k.IdentityApi.ListIdentities(ctx).Execute()
	if err != nil {
		return err
	}

	response := []openapi.APIKey{}
	for _, identity := range identities {
		if identity.SchemaId != kratosx.SchemaIDRobot {
			continue
		}

		apiKey, err := identityToAPIKey(identity)
		if err != nil {
			return err
		}

		response = append(response, apiKey)
	}

	return c.JSON(http.StatusOK, response)
}

func (server) GetApiKey(c echo.Context, apikey string) error {
	ctx := c.Request().Context()

	k := config.C.Kratos(true)
	identity, _, err := k.IdentityApi.GetIdentity(ctx, apikey).Execute()
	if err != nil {
		return err
	}

	resp, err := identityToAPIKey(*identity)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resp)
}

func (server) CreateApiKey(c echo.Context) error {
	log.Debug("creating api key")

	ctx := c.Request().Context()

	key, secret, err := generateAPIKeyPair()
	if err != nil {
		return err
	}

	if _, err := kratosx.AddApiKey(ctx, key, secret); err != nil {
		return fmt.Errorf("error adding API key to Kratos: %v", err)
	}

	return c.JSON(http.StatusCreated, openapi.APIKeyAndSecret{
		ApiKey: key,
		Secret: secret,
	})
}

// identityToAPIKey converts a kratos Identity to a Person
func identityToAPIKey(identity kratos.Identity) (openapi.APIKey, error) {
	traits, ok := identity.Traits.(map[string]interface{})
	if !ok {
		log.Debug("api key: %+V", identity.Traits)

		return openapi.APIKey{}, errors.New("unable to parse traits as robot! this should never happen")
	}

	key := openapi.APIKey{}

	if apikey := traits["apikey"]; apikey != nil {
		key.ApiKey = apikey.(string)
	}

	return key, nil
}

const (
	keyCharSet    = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	apiKeySize    = 10
	apiSecretSize = 15
)

func generateAPIKeyPair() (string, string, error) {
	key := ""
	for len(key) < apiKeySize {
		ch, err := rand.Int(rand.Reader, big.NewInt(int64(len(keyCharSet))))
		if err != nil {
			return "", "", err
		}
		key = key + string(keyCharSet[ch.Int64()])
	}

	secret := ""
	for len(secret) < apiSecretSize {
		ch, err := rand.Int(rand.Reader, big.NewInt(int64(len(keyCharSet))))
		if err != nil {
			return "", "", err
		}
		secret = secret + string(keyCharSet[ch.Int64()])
	}

	return key, secret, nil
}
