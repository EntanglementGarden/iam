#!/bin/bash
set -exuo pipefail
mkdir -p testdata/keto
chmod a+rwx testdata/keto

# start keto
podman run --rm --name iam-test-keto-migrate -v $(pwd)/tests/integration/ory:/etc/ory:ro -v $(pwd)/testdata:/var/ory oryd/keto:v0.11.1 migrate up -c /etc/ory/keto/keto.yaml -y
podman run --rm --name iam-test-keto -v $(pwd)/tests/integration/ory:/etc/ory:ro -v $(pwd)/testdata:/var/ory -p 127.0.0.1:4466:4466 -p 127.0.0.1:4467:4467 -d oryd/keto:v0.11.1 serve --sqa-opt-out -c /etc/ory/keto/keto.yaml
until curl -f http://localhost:4466/health/ready; do
    echo "waiting for keto to start..."
    sleep .5
done

export KETO_PORT_4466_TCP_ADDR=localhost
export KETO_PORT_4467_TCP_ADDR=localhost

# run tests
exitcode=0
if [[ "${1:-}" == "-d" ]]; then
    dlv test --headless -l 127.0.0.1:2345 --api-version=2 ./tests/integration/... || exitcode=$?
else
    go test ./... || exitcode=$?
fi

# clean up
podman logs iam-test-keto &> tests/integration/keto.log
podman stop iam-test-keto

rm -rf testdata


exit $exitcode
