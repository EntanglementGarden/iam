// Copyright 2021 Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	keto "github.com/ory/keto-client-go"
	kratos "github.com/ory/kratos-client-go"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/common/config"
)

// Config describes all possible config options
type Config struct {
	APIServerBind   string                     `json:"api_server_bind"`
	FormServerBind  string                     `json:"forms_server_bind"`
	KratosServer    kratos.ServerConfiguration `json:"kratos"`
	KratosAdmin     kratos.ServerConfiguration `json:"kratos_admin"`
	KetoReadServer  keto.ServerConfigurations  `json:"keto_read_server"`
	KetoWriteServer keto.ServerConfigurations  `json:"keto_write_server"`
	SqlitePath      string                     `json:"sqlite_path"`
}

// C is the configuration to be read at runtime. default values shown here
var C = Config{
	APIServerBind:   ":8080",
	FormServerBind:  ":8081",
	KratosServer:    kratos.ServerConfiguration{URL: "http://ory-kratos.entanglement-garden-iam.svc.cluster.local:4433"},
	KratosAdmin:     kratos.ServerConfiguration{URL: "http://ory-kratos.entanglement-garden-iam.svc.cluster.local:4434"},
	KetoReadServer:  keto.ServerConfigurations{{URL: "http://ory-keto.entanglement-garden-iam.svc.cluster.local:4466"}},
	KetoWriteServer: keto.ServerConfigurations{{URL: "http://ory-keto.entanglement-garden-iam.svc.cluster.local:4467"}},
	SqlitePath:      "/var/entanglement.garden/iam/iam.db",
}

// Load the config
func Load() {
	if err := config.Load("entanglement.garden", "iam", &C); err != nil {
		log.Fatal("error loading config: ", err)
	}
}

// Kratos builds a new Kratos API client for the configured Kratos server
func (c Config) Kratos(admin bool) *kratos.APIClient {
	kratosConfig := kratos.NewConfiguration()
	kratosConfig.UserAgent = egapi.GetUserAgent()
	if admin {
		kratosConfig.Servers = []kratos.ServerConfiguration{c.KratosAdmin}
	} else {
		kratosConfig.Servers = []kratos.ServerConfiguration{c.KratosServer}
	}
	return kratos.NewAPIClient(kratosConfig)
}

func (c Config) Keto(write bool) *keto.APIClient {
	ketoConfig := keto.NewConfiguration()
	ketoConfig.UserAgent = egapi.GetUserAgent()
	if write {
		ketoConfig.Servers = c.KetoWriteServer
	} else {
		ketoConfig.Servers = c.KetoReadServer
	}

	return keto.NewAPIClient(ketoConfig)
}
